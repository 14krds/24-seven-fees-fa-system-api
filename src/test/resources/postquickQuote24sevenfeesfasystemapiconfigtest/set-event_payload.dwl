{
  "line1": "123 Test St",
  "line2": "",
  "state": "AZ",
  "county": "Pima",
  "city": "Ajo",
  "zip": "12345",
  "transactionType": "Sale w/ Mortgage",
  "salesAmount": 500000,
  "loanAmount": 425000,
  "effectiveDate": "06/03/2020",
  "propertyType": "Residential"
}
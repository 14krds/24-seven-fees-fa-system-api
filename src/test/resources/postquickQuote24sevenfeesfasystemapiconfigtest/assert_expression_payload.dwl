%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo([
  {
    "underwriter": {
      "type": "Underwriter Rates",
      "vendor": "First American Underwriter Rates",
      "vendorUuid": "3b5e4c74-c17e-4a5c-8d4a-f345b0434ebf",
      "isAffiliate": false,
      "lineItems": [
        {
          "name": "Eagle Owner's Policy",
          "amount": 1099.0,
          "split": {
            "buyer": 0.0,
            "seller": 1099.0,
            "other": 0
          },
          "actualAmount": 2000.0,
          "actualSplit": {
            "buyer": 0.0,
            "seller": 2000.0,
            "other": 0
          },
          "feeInfo": {
            "FeeActualTotalAmount": "2000.0",
            "FeeDescription": "Eagle Owner's Policy",
            "FeePaidToType": "ThirdPartyProvider",
            "FeeType": "TitleOwnersCoveragePremium",
            "GFEDisclosedFeeAmount": "1099.0",
            "DisclosureSectionNumber": "H",
            "DisclosureSectionName": "Owner's Title Insurance",
            "DisclosureItemName": "Title - Owner's Title Insurance (optional) (Eagle Owner's Policy)",
            "DisclosureHUDLine": "HUD-1 Line 1103"
          }
        },
        {
          "name": "ALTA Loan Policy - Extended",
          "amount": 1967.0,
          "split": {
            "buyer": 1967.0,
            "seller": 0.0,
            "other": 0
          },
          "actualAmount": 1066.0,
          "actualSplit": {
            "buyer": 1066.0,
            "seller": 0.0,
            "other": 0
          },
          "feeInfo": {
            "FeeActualTotalAmount": "1066.0",
            "FeeDescription": "ALTA Loan Policy - Extended",
            "FeePaidToType": "ThirdPartyProvider",
            "FeeType": "TitleLendersCoveragePremium",
            "GFEDisclosedFeeAmount": "1967.0",
            "DisclosureSectionNumber": "B or C",
            "DisclosureSectionName": "Lender's Title Insurance",
            "DisclosureItemName": "Title - Lender’s Title Insurance(ALTA Loan Policy - Extended)",
            "DisclosureHUDLine": "HUD-1 Line 1104"
          }
        }
      ],
      "disclaimer": [
        "<b style='font-family:Calibri;color:steelblue'>Disclaimer Regarding Simultaneous Title Insurance Premium Rate in Purchase Transactions:</b> </br>For most policies, in order to comply with federal consumer protection laws, including, but not limited to, the Truth in Lending Act, the Real Estate Settlement Procedures Act, and the regulations and other guidance promulgated pursuant thereto (see: 12 CFR Part 1026 – Supplement I – comments 37(f)(2)-4, 37(g)(4)-2, 38(f)(2)-1, and 38(g)(4)-2), the premium when a special rate may be available based on the simultaneous issuance of a loan policy and an owner’s policy will be calculated and disclosed as follows:  </br>   1.  The title insurance premium for a lender’s title policy is calculated using the full rate based on the principal of the loan amount.  </br>   2.  The  title insurance premium for an owner’s policy is calculated using the full rate based on the full market value/purchase price, adding the simultaneous issuance premium for the lender’s coverage, and then subtracting the full premium for lender’s coverage (as calculated in item 1 above). </br> <b style='font-family:Calibri;color:steelblue'>Disclaimer:</b></br>The First American Comprehensive Calculator (FACC) is an Internet-based platform, which provides our customers with a user-friendly method of obtaining estimates for certain categories of settlement related costs. There may be variables that need to be considered in determining the final rate to be charged, including geographic and transaction-specific items, which are beyond the functionality provided by the FACC. All estimates obtained through the use of this calculator are dependent upon the accuracy of the information entered into the calculator and no guarantee of issuance is expressed or implied. Please contact your local First American office or agent to confirm your quote. Contact information for First American offices and agents in your area is available at <a href=\"http://www.firstam.com\" target=\"_blank\">www.firstam.com.</a> </br> </br>"
      ]
    }
  },
  {
    "recording": {
      "type": "Recording Fees",
      "vendor": "First American Recording Fees",
      "vendorUuid": "3a9de2d0-9a47-4da2-a544-38882169597d",
      "isAffiliate": false,
      "lineItems": [
        {
          "name": "Mortgage (Deed of Trust) - RecordingFee",
          "amount": 30.0,
          "split": {
            "buyer": 0.0,
            "seller": 0.0,
            "other": 30.0
          },
          "feeInfo": {
            "FeeActualTotalAmount": "30.0",
            "FeeDescription": "RecordingFee",
            "FeePaidToType": "ThirdPartyProvider",
            "FeeType": "RecordingFeeForMortgage",
            "DisclosureSectionNumber": "E",
            "DisclosureSectionName": "Taxes and other Government Fees Section",
            "DisclosureItemName": "Mortgage (Deed of Trust) - Recording Fee",
            "DisclosureHUDLine": "HUD-1 Line 1201"
          },
          "feePaidTo": "Pima County Recorder"
        },
        {
          "name": "Conveyance Deed - RecordingFee",
          "amount": 30.0,
          "split": {
            "buyer": 0.0,
            "seller": 0.0,
            "other": 30.0
          },
          "feeInfo": {
            "FeeActualTotalAmount": "30.0",
            "FeeDescription": "RecordingFee",
            "FeePaidToType": "ThirdPartyProvider",
            "FeeType": "RecordingFeeForDeed",
            "DisclosureSectionNumber": "E",
            "DisclosureSectionName": "Taxes and other Government Fees Section",
            "DisclosureItemName": "Conveyance Deed - Recording Fee",
            "DisclosureHUDLine": "HUD-1 Line 1201"
          },
          "feePaidTo": "Pima County Recorder"
        }
      ]
    }
  },
  {
    "settlement": {
      "type": "Settlement Fees",
      "vendor": "First American Settlement Fees",
      "vendorUuid": "12a83d9b-6f5a-40f3-9f03-173660562606",
      "isAffiliate": false,
      "lineItems": [
        {
          "name": "Title - Basic Escrow Fee (Sale and Loan Fee)",
          "amount": 1526.0,
          "split": {
            "buyer": 763.0,
            "seller": 763.0,
            "other": 0
          },
          "feeInfo": {
            "FeeActualTotalAmount": "1526.0",
            "FeeDescription": "ClosingFee",
            "FeePaidToType": "ThirdPartyProvider",
            "FeeType": "EscrowServiceFee",
            "DisclosureSectionNumber": "B or C",
            "DisclosureSectionName": "Lender's Title Insurance",
            "DisclosureItemName": "Title - Basic Escrow Fee (Sale and Loan Fee)",
            "DisclosureHUDLine": "HUD-1 Line 1101"
          }
        }
      ]
    }
  }
])
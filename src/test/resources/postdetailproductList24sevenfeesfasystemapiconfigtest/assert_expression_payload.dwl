%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo({
  "availibleFees": [
    {
      "DefaultValue": "1",
      "Id": "16",
      "MaximumValue": "10",
      "Name": "Additional Work Charge",
      "Quantity": "1",
      "UnitType": "Hourly"
    },
    {
      "DefaultValue": "5",
      "Id": "281",
      "MaximumValue": "10",
      "Name": "Checks Issued Fee",
      "Quantity": "5",
      "UnitType": "Quantity"
    },
    {
      "DefaultValue": "2",
      "Id": "282",
      "MaximumValue": "10",
      "Name": "Demand Fee",
      "Quantity": "2",
      "UnitType": "Quantity"
    },
    {
      "DefaultValue": "0",
      "Id": "20",
      "MaximumValue": "10",
      "Name": "Holdbacks",
      "Quantity": "0",
      "UnitType": "Quantity"
    },
    {
      "DefaultValue": "1",
      "Id": "21",
      "MaximumValue": "1",
      "Name": "Interest Bearing Account",
      "Quantity": "1",
      "UnitType": "Quantity"
    },
    {
      "DefaultValue": "1",
      "Id": "18",
      "MaximumValue": "1",
      "Name": "Mobile Home Fee",
      "Quantity": "1",
      "UnitType": "Quantity"
    },
    {
      "DefaultValue": "0",
      "Id": "23",
      "MaximumValue": "10",
      "Name": "Overnight Delivery Fee",
      "Quantity": "0",
      "UnitType": "Quantity"
    },
    {
      "DefaultValue": "0",
      "Id": "24",
      "MaximumValue": "10",
      "Name": "Reconveyance Tracking Fee",
      "Quantity": "0",
      "UnitType": "Quantity"
    },
    {
      "DefaultValue": "0",
      "Id": "994",
      "MaximumValue": "3",
      "Name": "Additional Loans",
      "Quantity": "0",
      "UnitType": "Quantity"
    },
    {
      "DefaultValue": "1",
      "Id": "16",
      "MaximumValue": "10",
      "Name": "Additional Work Charge",
      "Quantity": "1",
      "UnitType": "Hourly"
    },
    {
      "DefaultValue": "2",
      "Id": "282",
      "MaximumValue": "10",
      "Name": "Demand Fee",
      "Quantity": "2",
      "UnitType": "Quantity"
    },
    {
      "DefaultValue": "0",
      "Id": "20",
      "MaximumValue": "10",
      "Name": "Holdbacks",
      "Quantity": "0",
      "UnitType": "Quantity"
    },
    {
      "DefaultValue": "1",
      "Id": "21",
      "MaximumValue": "1",
      "Name": "Interest Bearing Account",
      "Quantity": "1",
      "UnitType": "Quantity"
    },
    {
      "DefaultValue": "1",
      "Id": "18",
      "MaximumValue": "1",
      "Name": "Mobile Home Fee",
      "Quantity": "1",
      "UnitType": "Quantity"
    },
    {
      "DefaultValue": "0",
      "Id": "23",
      "MaximumValue": "10",
      "Name": "Overnight Delivery Fee",
      "Quantity": "0",
      "UnitType": "Quantity"
    },
    {
      "DefaultValue": "0",
      "Id": "24",
      "MaximumValue": "10",
      "Name": "Reconveyance Tracking Fee",
      "Quantity": "0",
      "UnitType": "Quantity"
    }
  ],
  "closingTypes": [
    {
      "closingName": "Fees Only - No Closing",
      "includedFees": {
        "DefaultValue": "1 (included)",
        "Id": "988",
        "MaximumValue": "1",
        "Name": "Fees Only - No Closing",
        "Quantity": "1",
        "UnitType": "Quantity"
      }
    },
    {
      "closingName": "Basic Escrow Fee (Sale and Loan Fee) - Residential",
      "includedFees": {
        "DefaultValue": "1",
        "Id": "12",
        "MaximumValue": "1",
        "Name": "Basic Escrow Fee (Sale and Loan Fee)",
        "Quantity": "0",
        "UnitType": "Quantity"
      }
    }
  ],
  "cpls": [
    {
      "id": "7",
      "name": "Closing Protection Letter - Borrower/Buyer",
      "isDefault": "false",
      "isRequired": "false",
      "policyCategoryIds": [
        "-1"
      ]
    },
    {
      "id": "3",
      "name": "Closing Protection Letter - Lender",
      "isDefault": "false",
      "isRequired": "false",
      "policyCategoryIds": [
        "-1"
      ]
    },
    {
      "id": "4",
      "name": "Closing Protection Letter - Seller",
      "isDefault": "false",
      "isRequired": "false",
      "policyCategoryIds": [
        "-1"
      ]
    }
  ],
  "endorsements": [
    {
      "id": "1",
      "name": "[ALTA 1-06 ] Street Assessments",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "3",
      "name": "[ALTA 2-06 ] Truth in Lending",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "4",
      "name": "[ALTA 3-06] Zoning",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2904",
      "name": "[ALTA 3.1-06] Zoning - Completed Structure",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8081",
      "name": "[ALTA 3.2-06] Zoning - Land Under Development",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "6",
      "name": "[ALTA 4-06] Condominium - Assessments Priority",
      "effectiveDate": "2018-02-01",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7",
      "name": "[ALTA 4.1-06] Condominium - Current Assessments",
      "effectiveDate": "2018-02-01",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8",
      "name": "[ALTA 5-06] Planned Unit Development - Assessments Priority",
      "effectiveDate": "2018-02-01",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "9",
      "name": "[ALTA 5.1-06] Planned Unit Development - Current Assessments",
      "effectiveDate": "2018-02-01",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "10",
      "name": "[ALTA 6-06] Variable Rate Mortgage",
      "effectiveDate": "2018-02-01",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "11",
      "name": "[ALTA 6.1-06] Variable Rate, Regulations",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "12",
      "name": "[ALTA 6.2-06] Variable Rate Mortgage - Negative Amortization",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "14",
      "name": "[ALTA 7-06 ] Manufactured Housing Unit",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "15",
      "name": "[ALTA 7.1-06] Manufactured Housing - Conversion - Loan",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "17",
      "name": "[ALTA 7.2-06] Manufactured Housing - Conversion - Owner's",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "16",
      "name": "[ALTA 8.1-06 ] Environmental Protection Lien",
      "effectiveDate": "2018-02-01",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "18",
      "name": "[ALTA 9-06] Restrictions, Encroachments, Minerals - Loan Policy",
      "effectiveDate": "2018-02-01",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "19",
      "name": "[ALTA 9.1-06] Covenants, Conditions and Restrictions - Unimproved Land - Owner's Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "20",
      "name": "[ALTA 9.2-06] Covenants, Conditions and Restrictions - Improved Land - Owner’s Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "21",
      "name": "[ALTA 9.3-06] Covenants, Conditions and Restrictions - Loan Policy",
      "effectiveDate": "2018-02-01",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8082",
      "name": "[ALTA 9.6-06] Private Rights - Loan Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8405",
      "name": "[ALTA 9.6.1-06] Private Rights - Current Assessments - Loan Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8083",
      "name": "[ALTA 9.7-06] Restrictions, Encroachments, Minerals - Land Under Development - Loan Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8090",
      "name": "[ALTA 9.8-06] Covenants, Conditions and Restrictions - Land Under Development - Owner’s Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8256",
      "name": "[ALTA 9.9-06] Private Rights - Owner's Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8257",
      "name": "[ALTA 9.10-06] Restrictions, Encroachments, Minerals - Current Violations - Loan Policy",
      "effectiveDate": "2018-02-01",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "24",
      "name": "[ALTA 10-06] Assignment",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "5952",
      "name": "[ALTA 10.1-06] Assignment and Date Down",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "26",
      "name": "[ALTA 11-06 ] Mortgage Modification",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "3572",
      "name": "[ALTA 11.1-06] Mortgage Modification with Subordination",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8285",
      "name": "[ALTA 11.2-06] Mortgage Modification with Additional Amount of Insurance",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "28",
      "name": "[ALTA 13-06] Leasehold - Owner’s Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "29",
      "name": "[ALTA 13.1-06] Leasehold - Loan Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "30",
      "name": "[ALTA 14-06 ] Future Advance - Priority",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "31",
      "name": "[ALTA 14.1-06 ] Future Advance - Knowledge",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "32",
      "name": "[ALTA 14.2-06 ] Future Advance - Letter of Credit",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "33",
      "name": "[ALTA 14.3-06] Future Advance Reverse Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "34",
      "name": "[ALTA 15-06 ] Non-Imputation - Full Equity Transfer",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1767",
      "name": "[ALTA 15.1-06] Nonimputation – Additional Insured",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "36",
      "name": "[ALTA 15.2-06 ] Non-Imputation - Partial Equity Transfer",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "38",
      "name": "[ALTA 17-06 ] Access and Entry",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "39",
      "name": "[ALTA 17.1-06 ] Indirect Access and Entry",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7396",
      "name": "[ALTA 17.2-06 ] Utility Access",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "40",
      "name": "[ALTA 18-06 ] Single Tax Parcel",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "41",
      "name": "[ALTA 18.1-06] Multiple Tax Parcel - Easements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "42",
      "name": "[ALTA 19-06 ] Contiguity - Multiple Parcels",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "43",
      "name": "[ALTA 19.1-06 ] Contiguity - Single Parcel",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8403",
      "name": "[ALTA 19.2-06] Contiguity - Specified Parcels",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "44",
      "name": "[ALTA 20-06 ] First Loss - Multiple Parcel Transactions",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "45",
      "name": "[ALTA 22-06] Location",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "46",
      "name": "[ALTA 22.1-06 ] Location and Map",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7398",
      "name": "[ALTA 24-06 ] Doing Business",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7399",
      "name": "[ALTA 25-06 ] Same as Survey",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7400",
      "name": "[ALTA 25.1-06 ] Same as Portion of Survey",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7401",
      "name": "[ALTA 26-06 ] Subdivision",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7402",
      "name": "[ALTA 27-06 ] Usury",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7403",
      "name": "[ALTA 28-06 ] Easement - Damage or Enforced Removal",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8084",
      "name": "[ALTA 28.1-06] Encroachments - Boundaries and Easements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8258",
      "name": "[ALTA 28.2-06] Encroachments - Boundaries and Easements - Described Improvements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8404",
      "name": "[ALTA 28.3-06] Encroachments - Boundaries and Easements - Land Under Development",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7869",
      "name": "[ALTA 30-06] Shared Appreciation Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7873",
      "name": "[ALTA 31-06] Severable Improvements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8042",
      "name": "[ALTA 32-06] Construction Loan",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8043",
      "name": "[ALTA 32.1-06] Construction Loan - Direct Payment",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8149",
      "name": "[ALTA 32.2-06] Construction Loan - Insured's Direct Payment",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8044",
      "name": "[ALTA 33-06] Disbursement",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8066",
      "name": "[ALTA 34-06] Identified Risk Coverage",
      "effectiveDate": "2016-06-01",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8103",
      "name": "[ALTA 35-06] Minerals And Other Subsurface Substances - Buildings",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8104",
      "name": "[ALTA 35.1-06] Minerals And Other Subsurface Substances - Improvements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8105",
      "name": "[ALTA 35.2-06] Minerals And Other Subsurface Substances - Described Improvements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8106",
      "name": "[ALTA 35.3-06] Minerals And Other Subsurface Substances - Land Under Development",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8107",
      "name": "[ALTA 36-06] Energy Project - Leasehold/Easement - Owner’s Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8108",
      "name": "[ALTA 36.1-06] Energy Project - Leasehold/Easement - Loan Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8109",
      "name": "[ALTA 36.2-06] Energy Project - Leasehold - Owner’s Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8110",
      "name": "[ALTA 36.3-06] Energy Project - Leasehold - Loan Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8111",
      "name": "[ALTA 36.4-06] Energy Project - Covenants, Conditions and Restrictions - Land Under Dev. - Owner’s Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8112",
      "name": "[ALTA 36.5-06] Energy Project - Covenants, Conditions and Restrictions - Land Under Dev. - Loan",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8113",
      "name": "[ALTA 36.6-06] Energy Project - Encroachments",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8352",
      "name": "[ALTA 36.7-06] Energy Project - Fee Estate - Owner’s Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8353",
      "name": "[ALTA 36.8-06] Energy Project - Fee Estate - Loan Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8247",
      "name": "[ALTA 37-06] Assignment of Rents or Leases",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8260",
      "name": "[ALTA 38-06] Mortgage Tax",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8261",
      "name": "[ALTA 39-06] Policy Authentication",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8286",
      "name": "[ALTA 41-06] Water - Buildings",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8287",
      "name": "[ALTA 41.1-06] Water - Improvements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8288",
      "name": "[ALTA 41.2-06] Water - Described Improvements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8289",
      "name": "[ALTA 41.3-06] Water - Land Under Development",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8291",
      "name": "[ALTA 43-06] Anti-Taint - Loan Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8292",
      "name": "[ALTA 44-06] Insured Mortgage Recording - Loan",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8354",
      "name": "[ALTA 45-06] Pari Passu Mortgage - Loan Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "483",
      "name": "[AZ - NFE] Non Filed Endorsements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "-1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "108",
      "name": "[CLTA 100-06] Restrictions, Encroachments & Minerals",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "109",
      "name": "[CLTA 100.1] Restrictions, Encroachments & Minerals - Lender",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "110",
      "name": "[CLTA 100.2-06] Restrictions, Encroachments, Minerals",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8085",
      "name": "[CLTA 100.2.1-06] Covenants, Conditions and Restrictions - Loan Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "111",
      "name": "[CLTA 100.4-06 ] CC&R's, Violations",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "112",
      "name": "[CLTA 100.5-06 ] CC&R's, Violations",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "113",
      "name": "[CLTA 100.6-06] CC&R's, Including Future Violations - Owner's Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "114",
      "name": "[CLTA 100.7-06 ] CC&R's, Violations",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "115",
      "name": "[CLTA 100.8-06 ] CC&R's, Violations",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8088",
      "name": "[CLTA 100.9-06] Covenants, Conditions and Restrictions - Unimproved Land - Owner's Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8089",
      "name": "[CLTA 100.10-06] Covenants, Conditions and Restrictions - Improved Land - Owner's Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "116",
      "name": "[CLTA 100.12-06] CC&R's, Right of Reversion",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "117",
      "name": "[CLTA 100.13-06] CC&R's, Assessment Liens - ALTA Lender",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "118",
      "name": "[CLTA 100.17-06] CC&R's, Proper Modification",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "119",
      "name": "[CLTA 100.18-06] CC&R's, Right of Reversion",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "120",
      "name": "[CLTA 100.19-06] CC&R's, Violations",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "121",
      "name": "[CLTA 100.20-06] CC&R's, Violations",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "122",
      "name": "[CLTA 100.21-06] CC&R's, Plans and Specifications",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "123",
      "name": "[CLTA 100.23-06] Minerals, Surface Damage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "124",
      "name": "[CLTA 100.24-06] Minerals, Surface Entry by Damage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "125",
      "name": "[CLTA 100.25-06] Minerals, Surface Use",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "126",
      "name": "[CLTA 100.26-06] Minerals, Present-Future Improvements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "127",
      "name": "[CLTA 100.27-06] CC&R's, Violations",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "128",
      "name": "[CLTA 100.28-06] CC&R's, Violation - Future Improvements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7842",
      "name": "[CLTA 100.29-06] Minerals, Surface Damage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "130",
      "name": "[CLTA 101] Mechanic's Liens - CLTA Lender",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "131",
      "name": "[CLTA 101.1-06] Mechanics' Liens",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "132",
      "name": "[CLTA 101.2-06] Mechanics' Liens, Notice of Completion",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "133",
      "name": "[CLTA 101.3-06] Mechanics' Liens, No Notice of Completion",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "134",
      "name": "[CLTA 101.4] Mechanics' Liens, No Notice of Completion",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "135",
      "name": "[CLTA 101.5-06] Mechanics' Liens, Notice of Completion",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "136",
      "name": "[CLTA 101.6-06] Mechanics' Liens, Notice of Completion",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "137",
      "name": "[CLTA 101.8] Mechanic's Liens - Lender",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "138",
      "name": "[CLTA 101.9-06] Mechanics' Liens, Notice of Completion",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "139",
      "name": "[CLTA 101.10-06] Mechanics' Liens, Notice of Completion",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "140",
      "name": "[CLTA 101.11-06] Mechanics' Liens, No Notice of Completion",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "141",
      "name": "[CLTA 101.12-06] Mechanics' Liens, No Notice of Completion",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "142",
      "name": "[CLTA 101.13-06] Mechanics' Liens, Notice of Completion",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "143",
      "name": "[CLTA 102.4-06] Foundation",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "144",
      "name": "[CLTA 102.5-06] Foundation",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "145",
      "name": "[CLTA 102.6-06] Foundation, Portion of Premises",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "146",
      "name": "[CLTA 102.7-06] Foundation, Portion of Premises",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "147",
      "name": "[CLTA 103.1-06] Easement, Damage or Enforced Removal",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8071",
      "name": "[CLTA 103.2-06] Easement, Damage - Use or Maintenance",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "148",
      "name": "[CLTA 103.3-06] Easement, Existing Encroachment, Enforced Removal",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "149",
      "name": "[CLTA 103.4-06] Easement, Access to Public Street",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "150",
      "name": "[CLTA 103.5-06] Water Rights, Surface Damage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "151",
      "name": "[CLTA 103.6-06] Encroachments, None Exist",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "4125",
      "name": "[CLTA 103.7-06] Land Abuts Street",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "152",
      "name": "[CLTA 103.8-06] Water Rights, Future Improvements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "153",
      "name": "[CLTA 103.9-06] Encroachment, Future Improvements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "154",
      "name": "[CLTA 103.10-06] Surface Use, Horizontal Subdivision",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8067",
      "name": "[CLTA 103.11-06] Access and Entry",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8079",
      "name": "[CLTA 103.12-06] Indirect Access and Entry",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "802",
      "name": "[CLTA 104] Assignment of Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "156",
      "name": "[CLTA 104.A] Assignment of Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "689",
      "name": "[CLTA 104.1] Assignment of Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "158",
      "name": "[CLTA 104.4-06] Collateral Assignment of Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "159",
      "name": "[CLTA 104.6-06] Assignment of Rents or Leases",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "160",
      "name": "[CLTA 104.7-06] Assignment of Rents/Leases",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "161",
      "name": "[CLTA 104.8-06 ] Assignment of Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "162",
      "name": "[CLTA 104.9] Assignment of Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "163",
      "name": "[CLTA 104.10-06 ] Assignment of Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "164",
      "name": "[CLTA 104.11-06 ] Collateral Assignment of Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "165",
      "name": "[CLTA 104.12-06] Assignment",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "166",
      "name": "[CLTA 104.13-06 ] Assignment and Date Down",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "167",
      "name": "[CLTA 105-06] Multiple Mortgages in One Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "175",
      "name": "[CLTA 107.1-06] Allocation of Liability of Parcels",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "176",
      "name": "[CLTA 107.2-06 ] Amount of Insurance, Increase",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "177",
      "name": "[CLTA 107.5-06] Leasehold Improvements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "178",
      "name": "[CLTA 107.9-06 ] Additional Insured",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "179",
      "name": "[CLTA 107.10-06 ] Additional Insured",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "180",
      "name": "[CLTA 107.11-06] Non-Merger After Lender Acquires Title",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "184",
      "name": "[CLTA 108.10-06] Revolving Credit Loan, Increase",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "185",
      "name": "[CLTA 109 ] Oil and Gas Lease, No Assignments",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "186",
      "name": "[CLTA 110.1-06] Deletion of Item From Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "187",
      "name": "[CLTA 110.3-06] Minerals, Conveyance of Surface Rights",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "753",
      "name": "[CLTA 110.4] Modification of Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "693",
      "name": "[CLTA 110.5] Modification of Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "190",
      "name": "[CLTA 110.6] Modification of Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "191",
      "name": "[CLTA 110.7-06] Insurance Against Enforceability of Item",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "192",
      "name": "[CLTA 110.9-06] Environmental Protection Lien",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "193",
      "name": "[CLTA 110.10-06 ] Modification and Additional Advance",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8091",
      "name": "[CLTA 110.11-06] Mortgage Modification",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8068",
      "name": "[CLTA 110.11.1-06] Mortgage Modification with Subordination",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "194",
      "name": "[CLTA 111-06] Mortgage Priority, Partial Reconveyance",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "195",
      "name": "[CLTA 111.1-06] Mortgage Priority, Partial Reconveyance",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "196",
      "name": "[CLTA 111.2-06] Mortgage Priority, Subordination",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "197",
      "name": "[CLTA 111.3-06] Mortgage Priority, Encroachment, Address",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "198",
      "name": "[CLTA 111.4-06] Mortgage Impairment After Conveyance",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "199",
      "name": "[CLTA 111.5-06] Variable Rate Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "201",
      "name": "[CLTA 111.7-06] Variable Rate, Renewal",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "202",
      "name": "[CLTA 111.8-06] Variable Rate Mortgage - Negative Amortization",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "203",
      "name": "[CLTA 111.9-06] Variable Rate, FNMA 7 Year Balloon",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "204",
      "name": "[CLTA 111.10-06] Revolving Credit Loan, Optional Advance",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "205",
      "name": "[CLTA 111.11-06] Revolving Credit Loan, Obligatory Advance",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7367",
      "name": "[CLTA 111.14] Future Advance - Priority",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7369",
      "name": "[CLTA 111.14.1 ] Future Advance - Knowledge",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7371",
      "name": "[CLTA 111.14.2 ] Future Advance - Letter of Credit",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8541",
      "name": "[CLTA 111.14.3] Future Advance – Reverse Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8486",
      "name": "[CLTA 112.1B-06] Revolving Credit Loan, Obligatory Advance",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "209",
      "name": "[CLTA 114-06] Co-insurance",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "210",
      "name": "[CLTA 114.1-06] Co-Insurance, Joint and Several Liability",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "211",
      "name": "[CLTA 114.2-06] Co-Insurance, Joint and Several Liability",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "212",
      "name": "[CLTA 115-06] Condominium",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2564",
      "name": "[CLTA 115.1] Condominium",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "213",
      "name": "[CLTA 115.1-06] Condominium - Assessments Priority",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "-1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2565",
      "name": "[CLTA 115.2] Planned Unit Development",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7894",
      "name": "[CLTA 115.3] Condominium",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7378",
      "name": "[CLTA 115.3-06] Condominium - Current Assessments",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "215",
      "name": "[CLTA 116-06] Designation of Improvements, Address",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "216",
      "name": "[CLTA 116.1-06] Same as Survey",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7379",
      "name": "[CLTA 116.01-06 ] Location",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8092",
      "name": "[CLTA 116.1.2-06] Same as Portion of Survey",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7380",
      "name": "[CLTA 116.02-06 ] Location and Map",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "217",
      "name": "[CLTA 116.2-06] Designation of Improvements, Condominium",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "218",
      "name": "[CLTA 116.3-06] Legal Description, New Subdivision",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "219",
      "name": "[CLTA 116.4-06] Contiguity, Single Parcel",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "220",
      "name": "[CLTA 116.4.1-06] Contiguity, Multiple Parcels",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "221",
      "name": "[CLTA 116.5-06] Manufactured Housing Unit",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "222",
      "name": "[CLTA 116.5.1-06] Manufactured Housing - Conversion; Loan",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "223",
      "name": "[CLTA 116.5.2-06] Manufactured Housing - Conversion; Owners",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "224",
      "name": "[CLTA 116.6-06] Manufactured Housing Unit",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "225",
      "name": "[CLTA 116.7-06] Subdivision Map Act Compliance",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8080",
      "name": "[CLTA 116.8-06] Subdivision",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "226",
      "name": "[CLTA 119-06] Validity of Lease in Schedule B",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2575",
      "name": "[CLTA 119.1] Leasehold Policy, Additional Exceptions",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "228",
      "name": "[CLTA 119.2-06] Validity and Priority of Lease",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "229",
      "name": "[CLTA 119.3-06] Priority of Lease",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2578",
      "name": "[CLTA 119.4] Validity of Sublease, Joint Powers",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "4196",
      "name": "[CLTA 119.5-06] Leasehold - Owner",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7385",
      "name": "[CLTA 119.6-06] Leasehold - Loan",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1715",
      "name": "[CLTA 120.2] DOT Subordinate to Lease",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "232",
      "name": "[CLTA 122-06] Construction Lender Advance",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8487",
      "name": "[CLTA 122.1A-06] Construction Loan Advance-Initial Advance-2006",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "233",
      "name": "[CLTA 122.2-06] Construction Lender Advance",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1386",
      "name": "[CLTA 123.1] Zoning - Unimproved Land",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "234",
      "name": "[CLTA 123.1-06] Zoning, Unimproved Land",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1388",
      "name": "[CLTA 123.2] Zoning - Improved Land",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "235",
      "name": "[CLTA 123.2-06] Zoning, Completed Structure",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8369",
      "name": "[CLTA 123.3-06] Zoning - Land Under Development",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "236",
      "name": "[CLTA 124.1-06] Covenants are Binding",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "237",
      "name": "[CLTA 124.2-06] Covenants in Lease are Binding",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "238",
      "name": "[CLTA 124.3-06] Covenants in Lease are Binding",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8093",
      "name": "[CLTA 127-06] Nonimputation - Partial Equity Transfer",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8094",
      "name": "[CLTA 127.1-06] Nonimputation - Full Equity Transfer",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8095",
      "name": "[CLTA 127.2-06] Nonimputation - Additional Insured",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8096",
      "name": "[CLTA 129-06] Single Tax Parcel",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "7393",
      "name": "[CLTA 129.1-06] Multiple Tax Parcel - Easements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8097",
      "name": "[CLTA 130-06] First Loss, Multiple Parcel Transactions",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "246",
      "name": "[CLTA 132-06] Usury",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8098",
      "name": "[CLTA 133-06] Doing Business",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8101",
      "name": "[CLTA 135-06] One to Four Family Shared Appreciation",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8373",
      "name": "[CLTA 136-06] Severable Improvements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8374",
      "name": "[CLTA 137-06] Construction Loan - Loss of Priority",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8375",
      "name": "[CLTA 137.1-06] Construction Loan - Loss of Priority - Direct Payment",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "-1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8376",
      "name": "[CLTA 137.2-06] Construction Loan - Loss of Priority - Insured's Direct Payment",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8377",
      "name": "[CLTA 138-06] Disbursement",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8378",
      "name": "[CLTA 140-06] Minerals And Other Subsurface Substances - Buildings",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8379",
      "name": "[CLTA 140.1-06] Minerals And Other Subsurface Substances - Improvements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8380",
      "name": "[CLTA 140.2-06] Minerals And Other Subsurface Substances - Described Improvements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8381",
      "name": "[CLTA 140.3-06] Minerals And Other Subsurface Substances - Land Under Development",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8382",
      "name": "[CLTA 141-06] Energy Project - Leasehold/ Easement - Owner’s",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8383",
      "name": "[CLTA 141.1-06] Energy Project - Leasehold/ Easement - Loan",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8384",
      "name": "[CLTA 141.2-06] Energy Project - Leasehold - Owner’s",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8385",
      "name": "[CLTA 141.3-06] Energy Project - Leasehold - Loan",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8386",
      "name": "[CLTA 141.4-06] Energy Project - Covenants, Conditions and Restrictions - Land Under Dev. - Owner’s",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8387",
      "name": "[CLTA 141.5-06] Energy Project - Covenants, Conditions and Restrictions - Land Under Dev. - Loan",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8388",
      "name": "[CLTA 141.6-06] Energy Project - Encroachments",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8389",
      "name": "[CLTA 142-06] Policy Authentication",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8390",
      "name": "[CLTA 143-06] Water-Buildings",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8391",
      "name": "[CLTA 143.1-06] Water-Improvements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8392",
      "name": "[CLTA 143.2-06] Water-Described Improvements",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8393",
      "name": "[CLTA 143.3-06] Water-Land Under Development",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8395",
      "name": "[CLTA 145-06] Anti-Taint",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8396",
      "name": "[CLTA 146-06] Insured Mortgage Recording-Loan",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "49",
      "name": "[FA 1] Policy Correction - Deletion/Paragraph/Substitution",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "50",
      "name": "[FA 2] Encroachment onto Easement - DVA",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "51",
      "name": "[FA 3] Minerals - Surface Damage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "52",
      "name": "[FA 3.A] Minerals - Surface Damage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "53",
      "name": "[FA 4] Reservation, Use or Maintenance",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1357",
      "name": "[FA 5] Modification of Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "340",
      "name": "[FA 5.1 ] Future Modification of Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "54",
      "name": "[FA 6] No Improvements, Partial Reconveyance",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "56",
      "name": "[FA 8] Zoning",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2042",
      "name": "[FA 10] Assignment of Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "57",
      "name": "[FA 13] Manufactured Housing Unit",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "60",
      "name": "[FA 14] Leasehold Policy Conversion",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "61",
      "name": "[FA 15] Leasehold Policy Conversion",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "62",
      "name": "[FA 15.1 ] Leasehold Policy Conversion",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "63",
      "name": "[FA 16] Leasehold Policy Conversion",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "64",
      "name": "[FA 16.1 ] Leasehold Policy Conversion",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "65",
      "name": "[FA 20] Shared Appreciation Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2057",
      "name": "[FA 21.1 ] Revolving Credit Loan",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "67",
      "name": "[FA 22.1 ] Revolving Credit Loan",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "68",
      "name": "[FA 22.1A] Revolving Credit Loan",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "69",
      "name": "[FA 23] Revolving Credit Loan",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "70",
      "name": "[FA 25] Revolving Credit Loan",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "71",
      "name": "[FA 25.A ] Revolving Credit Loan",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "72",
      "name": "[FA 26] Revolving Credit Loan - Optional Advance",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2067",
      "name": "[FA 27] Environmental Lien, 1984 Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2068",
      "name": "[FA 27.1 ] Exclusions From Coverage, 1984 Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "73",
      "name": "[FA 28] Revolving Credit Loan",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "74",
      "name": "[FA 28.A ] Revolving Credit Loan",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "84",
      "name": "[FA 29] Revolving Credit Loan - Increase",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "85",
      "name": "[FA 29.1] Revolving Credit Loan - Increase",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "86",
      "name": "[FA 30.1 ] Leasehold Policy Conversion",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "87",
      "name": "[FA 31] Restrictions, Easements, Minerals - Unimproved Land",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1798",
      "name": "[FA 31.1] REM Unimproved",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "89",
      "name": "[FA 31.2] Restrictions, Easements, Minerals - Unimproved Land, Owner",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "90",
      "name": "[FA 32] Land Same as Map Attached to Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1254",
      "name": "[FA 32.1 ] Designation of Improvements, Address",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "91",
      "name": "[FA 33] Truth in Lending - ALTA Policies",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "92",
      "name": "[FA 35] Environmental Protection Lien",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "93",
      "name": "[FA 36] Variable Rate - Negative Amortization",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "94",
      "name": "[FA 36.1] Variable Rate FNMA 7 Year Balloon",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "95",
      "name": "[FA 37] Co-Insurance Clause - No Subsequent Improvement",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "96",
      "name": "[FA 38] Arbitration Clause, Modification",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "97",
      "name": "[FA 38.1 ] Arbitration Clause, Modification",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "98",
      "name": "[FA 39] Arbitration Clause, Modification",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1156",
      "name": "[FA 41] Assignment of Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2105",
      "name": "[FA 42] Restrictions, Encroachments & Minerals",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "690",
      "name": "[FA 43] Easements in Declaration",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "691",
      "name": "[FA 43.1 ] Leasehold - Interference in Use",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "696",
      "name": "[FA 43.2 ] Leasehold - Interference in Use",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "100",
      "name": "[FA 46] Express Policy, Assignment of Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "101",
      "name": "[FA 47] Modification of Mortgage, Limited",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "102",
      "name": "[FA 48] Modification of Mortgage, Limited",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "103",
      "name": "[FA 48.1 ] Modification of Mortgage, Limited",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1919",
      "name": "[FA 50.1] First Loss",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "338",
      "name": "[FA 50.2 ] First Loss Payable",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1920",
      "name": "[FA 50.3] First Loss",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "339",
      "name": "[FA 51 ] Last Dollar",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2124",
      "name": "[FA 51.1 ] Last Dollar",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2125",
      "name": "[FA 51.2 ] Last Dollar",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1922",
      "name": "[FA 52] Non-Imputation",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1923",
      "name": "[FA 52.1] Non-Imputation/Partnership",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2128",
      "name": "[FA 53] Litigation Guarantee Amendment",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2129",
      "name": "[FA 54] Litigation Guarantee Amendment",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "343",
      "name": "[FA 55] Fairway",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "706",
      "name": "[FA 55.1 ] Fairway and Successor Insured",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "344",
      "name": "[FA 56] Reverse Mortgage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1372",
      "name": "[FA 57] Usury I",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1373",
      "name": "[FA 57.1 ] Usury II",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "345",
      "name": "[FA 58] Doing Business - Lender",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2136",
      "name": "[FA 59] Anti-Taint",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2138",
      "name": "[FA 60] Recharacterization - Lender Only",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1021",
      "name": "[FA 61] Construction Loan Pending Disbursement",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1022",
      "name": "[FA 61.1 ] Construction Loan Disbursement",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1023",
      "name": "[FA 61.2] F.A. Form 61.2 - Construction Loan - Reinstatement of Covered Risk 11 (a)",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2142",
      "name": "[FA 61.3 ] Pending Improvements - Owner",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2143",
      "name": "[FA 62] Pending Disbursement - 122",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "5935",
      "name": "[FA 63] Modification of DOT with Creditor's Rights",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2145",
      "name": "[FA 63.1 ] Mod. of Mortgage - Cred. Rights Excl.",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2146",
      "name": "[FA 63.2 ] Mod. of Mortgage - Cred. Rights Excl.",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "772",
      "name": "[FA 73] Tenant Umbrella Coverage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1375",
      "name": "[FA 74] Zoning",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "1376",
      "name": "[FA 75] Zoning - Completed Structure",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "2160",
      "name": "[FA 76] Expanded Survey Coverage",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "107",
      "name": "[FA 88] Reverse Mortgage - (Trust Mortgagor)",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8045",
      "name": "[FA 92] Deletion of Natural Person Limitation",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "47",
      "name": "[JR1] ALTA JR 1",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "48",
      "name": "[JR2] JR2 Future Advance",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "454",
      "name": "[LTAA 1] Assignment of Mortgage or Deed of Trust Endorsement(CLTA 104)",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "455",
      "name": "[LTAA 1.1] Assignment of Mortgage or Deed of Trust Endorsement",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "456",
      "name": "[LTAA 2] Additional Advance Endorsement",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "457",
      "name": "[LTAA 3] Restrictions, Encroachments & Minerals: ALTA Extended Lender",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8538",
      "name": "[LTAA 3R Mod] Restrictions, Encroachments & Minerals: ALTA Lender",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8901",
      "name": "[LTAA 3R] Restrictions, Encroachments & Minerals: ALTA Extended Lender",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "458",
      "name": "[LTAA 4] Assignment of Mortgage or Deed of Trust Endorsement (FNMA)",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "459",
      "name": "[LTAA 5] Type of Improvement (CLTA 116)",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8484",
      "name": "[LTAA 6] Variable Rate Mortgage, Regulation",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "460",
      "name": "[LTAA 7] Modification of Policy",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "461",
      "name": "[LTAA 9] Partial Release (CLTA 111)",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "463",
      "name": "[LTAA 10] Lender's Bringdown (No Liability Increase) (CLTA 110.5)",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "464",
      "name": "[LTAA 10.1] Lender's Bringdown (No Liability Increase) (CLTA 110.5)",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "465",
      "name": "[LTAA 12] Specific Subordination",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8481",
      "name": "[LTAA 14] Street Assessments",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "466",
      "name": "[LTAA 15] Trustee Sale Guarantee Update",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "4"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8482",
      "name": "[LTAA 16] Zoning, Unimproved Land",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8483",
      "name": "[LTAA 17] Zoning, Improved Land",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "6165",
      "name": "[LTAA 21]  Bringdown (Property Added/No Liability Increase)",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "1",
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "8485",
      "name": "[LTAA 23] Foundation",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    },
    {
      "id": "480",
      "name": "[LTAA 27] Collateral Assignment",
      "effectiveDate": "2017-08-05",
      "validPolicyIds": [
        "2"
      ],
      "concurrentEndorsements": null,
      "excludedEndorsements": null
    }
  ],
  "policies": [
    {
      "id": "469",
      "name": "ALTA Loan Policy",
      "defaultRateTypeId": "0",
      "isDefault": "false",
      "defaultEndorsements": null,
      "validRateTypes": [
        {
          "id": "1",
          "name": "Basic"
        },
        {
          "id": "35",
          "name": "Investor Rate"
        }
      ],
      "policyCoverageName": "Standard",
      "policyCoverageIds": "2",
      "policyCoverageId": "1",
      "policyCoverageName": "Standard"
    },
    {
      "id": "470",
      "name": "ALTA Loan Policy - Extended",
      "defaultRateTypeId": "0",
      "isDefault": "false",
      "defaultEndorsements": null,
      "validRateTypes": [
        {
          "id": "1",
          "name": "Basic"
        },
        {
          "id": "35",
          "name": "Investor Rate"
        }
      ],
      "policyCoverageName": "Extended",
      "policyCoverageIds": "2",
      "policyCoverageId": "2",
      "policyCoverageName": "Extended"
    },
    {
      "id": "471",
      "name": "ALTA Owner's Policy",
      "defaultRateTypeId": "0",
      "isDefault": "false",
      "defaultEndorsements": null,
      "validRateTypes": [
        {
          "id": "1",
          "name": "Basic"
        },
        {
          "id": "35",
          "name": "Investor Rate"
        },
        {
          "id": "281",
          "name": "Short Term Residential Non-Prepaid"
        },
        {
          "id": "282",
          "name": "Short Term Residential Prepaid"
        }
      ],
      "policyCoverageName": "Standard",
      "policyCoverageIds": "1",
      "policyCoverageId": "1",
      "policyCoverageName": "Standard"
    },
    {
      "id": "472",
      "name": "ALTA Owner's Policy - Extended",
      "defaultRateTypeId": "0",
      "isDefault": "false",
      "defaultEndorsements": null,
      "validRateTypes": [
        {
          "id": "1",
          "name": "Basic"
        },
        {
          "id": "35",
          "name": "Investor Rate"
        },
        {
          "id": "281",
          "name": "Short Term Residential Non-Prepaid"
        },
        {
          "id": "282",
          "name": "Short Term Residential Prepaid"
        }
      ],
      "policyCoverageName": "Extended",
      "policyCoverageIds": "1",
      "policyCoverageId": "2",
      "policyCoverageName": "Extended"
    },
    {
      "id": "332",
      "name": "ALTA Short Form EAGLE Loan Policy",
      "defaultRateTypeId": "0",
      "isDefault": "false",
      "defaultEndorsements": null,
      "validRateTypes": [
        {
          "id": "1",
          "name": "Basic"
        },
        {
          "id": "35",
          "name": "Investor Rate"
        }
      ],
      "policyCoverageName": "Eagle",
      "policyCoverageIds": "2",
      "policyCoverageId": "3",
      "policyCoverageName": "Eagle"
    },
    {
      "id": "474",
      "name": "ALTA Short Form Residential Loan Policy",
      "defaultRateTypeId": "0",
      "isDefault": "false",
      "defaultEndorsements": null,
      "validRateTypes": [
        {
          "id": "1",
          "name": "Basic"
        }
      ],
      "policyCoverageName": "Standard",
      "policyCoverageIds": "2",
      "policyCoverageId": "1",
      "policyCoverageName": "Standard"
    },
    {
      "id": "485",
      "name": "ALTA Short Form Residential Loan Policy - Extended",
      "defaultRateTypeId": "0",
      "isDefault": "false",
      "defaultEndorsements": null,
      "validRateTypes": [
        {
          "id": "1",
          "name": "Basic"
        },
        {
          "id": "35",
          "name": "Investor Rate"
        }
      ],
      "policyCoverageName": "Extended",
      "policyCoverageIds": "2",
      "policyCoverageId": "2",
      "policyCoverageName": "Extended"
    },
    {
      "id": "437",
      "name": "ALTA Standard U.S. Policy",
      "defaultRateTypeId": "0",
      "isDefault": "false",
      "defaultEndorsements": null,
      "validRateTypes": [
        {
          "id": "1",
          "name": "Basic"
        }
      ],
      "policyCoverageName": "Standard",
      "policyCoverageIds": "1",
      "policyCoverageId": "1",
      "policyCoverageName": "Standard"
    },
    {
      "id": "342",
      "name": "Eagle Loan Policy",
      "defaultRateTypeId": "0",
      "isDefault": "false",
      "defaultEndorsements": null,
      "validRateTypes": [
        {
          "id": "1",
          "name": "Basic"
        },
        {
          "id": "35",
          "name": "Investor Rate"
        }
      ],
      "policyCoverageName": "Eagle",
      "policyCoverageIds": "2",
      "policyCoverageId": "3",
      "policyCoverageName": "Eagle"
    },
    {
      "id": "429",
      "name": "Eagle Owner's Policy",
      "defaultRateTypeId": "1",
      "isDefault": "true",
      "defaultEndorsements": null,
      "validRateTypes": [
        {
          "id": "1",
          "name": "Basic"
        },
        {
          "id": "35",
          "name": "Investor Rate"
        },
        {
          "id": "281",
          "name": "Short Term Residential Non-Prepaid"
        },
        {
          "id": "282",
          "name": "Short Term Residential Prepaid"
        }
      ],
      "policyCoverageName": "Eagle",
      "policyCoverageIds": "1",
      "policyCoverageId": "3",
      "policyCoverageName": "Eagle"
    }
  ],
  "recording": [
    {
      "name": "Mortgage (Deed of Trust)",
      "docType": "MORTGAGE",
      "isDefault": "true",
      "numberOfPages": "25",
      "considerationAmount": "0"
    },
    {
      "name": "Conveyance Deed",
      "docType": "DEED",
      "isDefault": "true",
      "numberOfPages": "3",
      "considerationAmount": "0"
    },
    {
      "name": "Assignment",
      "docType": "ASSIGNMENT",
      "isDefault": "false",
      "numberOfPages": "0",
      "considerationAmount": "0"
    },
    {
      "name": "Satisfaction",
      "docType": "SATISFACTION",
      "isDefault": "false",
      "numberOfPages": "0",
      "considerationAmount": "0"
    },
    {
      "name": "Amendment (Modification)",
      "docType": "AMMENDMENT",
      "isDefault": "false",
      "numberOfPages": "0",
      "considerationAmount": "0"
    },
    {
      "name": "Subordination",
      "docType": "SUBORDINATION",
      "isDefault": "false",
      "numberOfPages": "0",
      "considerationAmount": "0"
    },
    {
      "name": "Power of Attorney",
      "docType": "POA",
      "isDefault": "false",
      "numberOfPages": "0",
      "considerationAmount": "0"
    },
    {
      "name": "UCC1-County",
      "docType": "UCC1COUNTY",
      "isDefault": "false",
      "numberOfPages": "0",
      "considerationAmount": "0"
    },
    {
      "name": "UCC3-County",
      "docType": "UCC3COUNTY",
      "isDefault": "false",
      "numberOfPages": "0",
      "considerationAmount": "0"
    },
    {
      "name": "UCC Termination-County",
      "docType": "UCCTERMINATIONCOUNTY",
      "isDefault": "false",
      "numberOfPages": "0",
      "considerationAmount": "0"
    },
    {
      "name": "Death Certificate",
      "docType": "DEATHCERTIFICATE",
      "isDefault": "false",
      "numberOfPages": "0",
      "considerationAmount": "0"
    },
    {
      "name": "Affidavit",
      "docType": "AFFIDAVIT",
      "isDefault": "false",
      "numberOfPages": "0",
      "considerationAmount": "0"
    },
    {
      "name": "UCC1-State",
      "docType": "UCC1STATE",
      "isDefault": "false",
      "numberOfPages": "0",
      "considerationAmount": "0"
    },
    {
      "name": "UCC3-State",
      "docType": "UCC3STATE",
      "isDefault": "false",
      "numberOfPages": "0",
      "considerationAmount": "0"
    },
    {
      "name": "UCC Termination-State",
      "docType": "UCCTERMINATIONSTATE",
      "isDefault": "false",
      "numberOfPages": "0",
      "considerationAmount": "0"
    }
  ],
  "notes": null
})
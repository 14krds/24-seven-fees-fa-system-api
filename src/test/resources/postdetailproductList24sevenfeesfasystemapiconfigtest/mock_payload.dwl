do {
  ns lvis http://services.firstam.com/lvis/v2.0
  ---
  {
    lvis#"LVIS_XML": do {
      ns lvis http://services.firstam.com/lvis/v2.0
      ---
      {
        lvis#"LVIS_ACK_NACK": do {
          ns lvis http://services.firstam.com/lvis/v2.0
          ---
          {
            lvis#"DateTime": "2020-06-26T10:10:13.6195839-07:00",
            lvis#"StatusCd": "1000",
            lvis#"StatusDescription": "Request Successfully Processed"
          }
        },
        lvis#"LVIS_HEADER": do {
          ns lvis http://services.firstam.com/lvis/v2.0
          ---
          {
            lvis#"LVISActionType": "ProductList",
            lvis#"ClientCustomerId": "VSPrf491368",
            lvis#"ClientUniqueRequestId": "6511"
          }
        },
        lvis#"LVIS_CALCULATOR_TYPE_DATA_RESPONSE": do {
          ns lvis http://services.firstam.com/lvis/v2.0
          ---
          {
            lvis#"CalcTypeData": do {
              ns lvis http://services.firstam.com/lvis/v2.0
              ---
              {
                lvis#"ProductsList": do {
                  ns lvis http://services.firstam.com/lvis/v2.0
                  ---
                  {
                    lvis#"ClosingCosts": do {
                      ns lvis http://services.firstam.com/lvis/v2.0
                      ---
                      {
                        lvis#"ClosingCost": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"AvailableFees": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"ClosingFee": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"DefaultValue": "1",
                                    lvis#"Id": "16",
                                    lvis#"MaximumValue": "10",
                                    lvis#"Name": "Additional Work Charge",
                                    lvis#"Quantity": "1",
                                    lvis#"UnitType": "Hourly"
                                  }
                                },
                                lvis#"ClosingFee": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"DefaultValue": "5",
                                    lvis#"Id": "281",
                                    lvis#"MaximumValue": "10",
                                    lvis#"Name": "Checks Issued Fee",
                                    lvis#"Quantity": "5",
                                    lvis#"UnitType": "Quantity"
                                  }
                                },
                                lvis#"ClosingFee": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"DefaultValue": "2",
                                    lvis#"Id": "282",
                                    lvis#"MaximumValue": "10",
                                    lvis#"Name": "Demand Fee",
                                    lvis#"Quantity": "2",
                                    lvis#"UnitType": "Quantity"
                                  }
                                },
                                lvis#"ClosingFee": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"DefaultValue": "0",
                                    lvis#"Id": "20",
                                    lvis#"MaximumValue": "10",
                                    lvis#"Name": "Holdbacks",
                                    lvis#"Quantity": "0",
                                    lvis#"UnitType": "Quantity"
                                  }
                                },
                                lvis#"ClosingFee": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"DefaultValue": "1",
                                    lvis#"Id": "21",
                                    lvis#"MaximumValue": "1",
                                    lvis#"Name": "Interest Bearing Account",
                                    lvis#"Quantity": "1",
                                    lvis#"UnitType": "Quantity"
                                  }
                                },
                                lvis#"ClosingFee": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"DefaultValue": "1",
                                    lvis#"Id": "18",
                                    lvis#"MaximumValue": "1",
                                    lvis#"Name": "Mobile Home Fee",
                                    lvis#"Quantity": "1",
                                    lvis#"UnitType": "Quantity"
                                  }
                                },
                                lvis#"ClosingFee": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"DefaultValue": "0",
                                    lvis#"Id": "23",
                                    lvis#"MaximumValue": "10",
                                    lvis#"Name": "Overnight Delivery Fee",
                                    lvis#"Quantity": "0",
                                    lvis#"UnitType": "Quantity"
                                  }
                                },
                                lvis#"ClosingFee": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"DefaultValue": "0",
                                    lvis#"Id": "24",
                                    lvis#"MaximumValue": "10",
                                    lvis#"Name": "Reconveyance Tracking Fee",
                                    lvis#"Quantity": "0",
                                    lvis#"UnitType": "Quantity"
                                  }
                                }
                              }
                            },
                            lvis#"ClosingId": "356",
                            lvis#"ClosingName": "Fees Only - No Closing",
                            lvis#"IncludedFees": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"ClosingFee": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"DefaultValue": "1 (included)",
                                    lvis#"Id": "988",
                                    lvis#"MaximumValue": "1",
                                    lvis#"Name": "Fees Only - No Closing",
                                    lvis#"Quantity": "1",
                                    lvis#"UnitType": "Quantity"
                                  }
                                }
                              }
                            },
                            lvis#"IsDefault": "false",
                            lvis#"Notes": null
                          }
                        },
                        lvis#"ClosingCost": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"AvailableFees": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"ClosingFee": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"DefaultValue": "0",
                                    lvis#"Id": "994",
                                    lvis#"MaximumValue": "3",
                                    lvis#"Name": "Additional Loans",
                                    lvis#"Quantity": "0",
                                    lvis#"UnitType": "Quantity"
                                  }
                                },
                                lvis#"ClosingFee": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"DefaultValue": "1",
                                    lvis#"Id": "16",
                                    lvis#"MaximumValue": "10",
                                    lvis#"Name": "Additional Work Charge",
                                    lvis#"Quantity": "1",
                                    lvis#"UnitType": "Hourly"
                                  }
                                },
                                lvis#"ClosingFee": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"DefaultValue": "2",
                                    lvis#"Id": "282",
                                    lvis#"MaximumValue": "10",
                                    lvis#"Name": "Demand Fee",
                                    lvis#"Quantity": "2",
                                    lvis#"UnitType": "Quantity"
                                  }
                                },
                                lvis#"ClosingFee": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"DefaultValue": "0",
                                    lvis#"Id": "20",
                                    lvis#"MaximumValue": "10",
                                    lvis#"Name": "Holdbacks",
                                    lvis#"Quantity": "0",
                                    lvis#"UnitType": "Quantity"
                                  }
                                },
                                lvis#"ClosingFee": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"DefaultValue": "1",
                                    lvis#"Id": "21",
                                    lvis#"MaximumValue": "1",
                                    lvis#"Name": "Interest Bearing Account",
                                    lvis#"Quantity": "1",
                                    lvis#"UnitType": "Quantity"
                                  }
                                },
                                lvis#"ClosingFee": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"DefaultValue": "1",
                                    lvis#"Id": "18",
                                    lvis#"MaximumValue": "1",
                                    lvis#"Name": "Mobile Home Fee",
                                    lvis#"Quantity": "1",
                                    lvis#"UnitType": "Quantity"
                                  }
                                },
                                lvis#"ClosingFee": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"DefaultValue": "0",
                                    lvis#"Id": "23",
                                    lvis#"MaximumValue": "10",
                                    lvis#"Name": "Overnight Delivery Fee",
                                    lvis#"Quantity": "0",
                                    lvis#"UnitType": "Quantity"
                                  }
                                },
                                lvis#"ClosingFee": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"DefaultValue": "0",
                                    lvis#"Id": "24",
                                    lvis#"MaximumValue": "10",
                                    lvis#"Name": "Reconveyance Tracking Fee",
                                    lvis#"Quantity": "0",
                                    lvis#"UnitType": "Quantity"
                                  }
                                }
                              }
                            },
                            lvis#"ClosingId": "6",
                            lvis#"ClosingName": "Basic Escrow Fee (Sale and Loan Fee) - Residential",
                            lvis#"IncludedFees": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"ClosingFee": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"DefaultValue": "1",
                                    lvis#"Id": "12",
                                    lvis#"MaximumValue": "1",
                                    lvis#"Name": "Basic Escrow Fee (Sale and Loan Fee)",
                                    lvis#"Quantity": "0",
                                    lvis#"UnitType": "Quantity"
                                  }
                                }
                              }
                            },
                            lvis#"IsDefault": "true",
                            lvis#"Notes": null
                          }
                        }
                      }
                    },
                    lvis#"CPLs": do {
                      ns lvis http://services.firstam.com/lvis/v2.0
                      ---
                      {
                        lvis#"CPLType": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"CPL_Id": "7",
                            lvis#"CPL_Name": "Closing Protection Letter - Borrower/Buyer",
                            lvis#"IsDefault": "false",
                            lvis#"IsRequired": "false",
                            lvis#"PolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "-1"
                              }
                            }
                          }
                        },
                        lvis#"CPLType": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"CPL_Id": "3",
                            lvis#"CPL_Name": "Closing Protection Letter - Lender",
                            lvis#"IsDefault": "false",
                            lvis#"IsRequired": "false",
                            lvis#"PolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "-1"
                              }
                            }
                          }
                        },
                        lvis#"CPLType": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"CPL_Id": "4",
                            lvis#"CPL_Name": "Closing Protection Letter - Seller",
                            lvis#"IsDefault": "false",
                            lvis#"IsRequired": "false",
                            lvis#"PolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "-1"
                              }
                            }
                          }
                        }
                      }
                    },
                    lvis#"Endorsements": do {
                      ns lvis http://services.firstam.com/lvis/v2.0
                      ---
                      {
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1",
                            lvis#"EndorsementName": "[ALTA 1-06 ] Street Assessments",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "3",
                            lvis#"EndorsementName": "[ALTA 2-06 ] Truth in Lending",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "4",
                            lvis#"EndorsementName": "[ALTA 3-06] Zoning",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2904",
                            lvis#"EndorsementName": "[ALTA 3.1-06] Zoning - Completed Structure",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8081",
                            lvis#"EndorsementName": "[ALTA 3.2-06] Zoning - Land Under Development",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "6",
                            lvis#"EndorsementName": "[ALTA 4-06] Condominium - Assessments Priority",
                            lvis#"EffectiveDate": "2018-02-01",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7",
                            lvis#"EndorsementName": "[ALTA 4.1-06] Condominium - Current Assessments",
                            lvis#"EffectiveDate": "2018-02-01",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8",
                            lvis#"EndorsementName": "[ALTA 5-06] Planned Unit Development - Assessments Priority",
                            lvis#"EffectiveDate": "2018-02-01",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "9",
                            lvis#"EndorsementName": "[ALTA 5.1-06] Planned Unit Development - Current Assessments",
                            lvis#"EffectiveDate": "2018-02-01",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "10",
                            lvis#"EndorsementName": "[ALTA 6-06] Variable Rate Mortgage",
                            lvis#"EffectiveDate": "2018-02-01",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "11",
                            lvis#"EndorsementName": "[ALTA 6.1-06] Variable Rate, Regulations",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "12",
                            lvis#"EndorsementName": "[ALTA 6.2-06] Variable Rate Mortgage - Negative Amortization",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "14",
                            lvis#"EndorsementName": "[ALTA 7-06 ] Manufactured Housing Unit",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "15",
                            lvis#"EndorsementName": "[ALTA 7.1-06] Manufactured Housing - Conversion - Loan",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "17",
                            lvis#"EndorsementName": "[ALTA 7.2-06] Manufactured Housing - Conversion - Owner's",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "16",
                            lvis#"EndorsementName": "[ALTA 8.1-06 ] Environmental Protection Lien",
                            lvis#"EffectiveDate": "2018-02-01",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "18",
                            lvis#"EndorsementName": "[ALTA 9-06] Restrictions, Encroachments, Minerals - Loan Policy",
                            lvis#"EffectiveDate": "2018-02-01",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "19",
                            lvis#"EndorsementName": "[ALTA 9.1-06] Covenants, Conditions and Restrictions - Unimproved Land - Owner's Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "20",
                            lvis#"EndorsementName": "[ALTA 9.2-06] Covenants, Conditions and Restrictions - Improved Land - Owner’s Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "21",
                            lvis#"EndorsementName": "[ALTA 9.3-06] Covenants, Conditions and Restrictions - Loan Policy",
                            lvis#"EffectiveDate": "2018-02-01",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "-1",
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8082",
                            lvis#"EndorsementName": "[ALTA 9.6-06] Private Rights - Loan Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8405",
                            lvis#"EndorsementName": "[ALTA 9.6.1-06] Private Rights - Current Assessments - Loan Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8083",
                            lvis#"EndorsementName": "[ALTA 9.7-06] Restrictions, Encroachments, Minerals - Land Under Development - Loan Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8090",
                            lvis#"EndorsementName": "[ALTA 9.8-06] Covenants, Conditions and Restrictions - Land Under Development - Owner’s Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8256",
                            lvis#"EndorsementName": "[ALTA 9.9-06] Private Rights - Owner's Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8257",
                            lvis#"EndorsementName": "[ALTA 9.10-06] Restrictions, Encroachments, Minerals - Current Violations - Loan Policy",
                            lvis#"EffectiveDate": "2018-02-01",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "24",
                            lvis#"EndorsementName": "[ALTA 10-06] Assignment",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "5952",
                            lvis#"EndorsementName": "[ALTA 10.1-06] Assignment and Date Down",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "26",
                            lvis#"EndorsementName": "[ALTA 11-06 ] Mortgage Modification",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "3572",
                            lvis#"EndorsementName": "[ALTA 11.1-06] Mortgage Modification with Subordination",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8285",
                            lvis#"EndorsementName": "[ALTA 11.2-06] Mortgage Modification with Additional Amount of Insurance",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "28",
                            lvis#"EndorsementName": "[ALTA 13-06] Leasehold - Owner’s Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "29",
                            lvis#"EndorsementName": "[ALTA 13.1-06] Leasehold - Loan Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "30",
                            lvis#"EndorsementName": "[ALTA 14-06 ] Future Advance - Priority",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "31",
                            lvis#"EndorsementName": "[ALTA 14.1-06 ] Future Advance - Knowledge",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "32",
                            lvis#"EndorsementName": "[ALTA 14.2-06 ] Future Advance - Letter of Credit",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "33",
                            lvis#"EndorsementName": "[ALTA 14.3-06] Future Advance Reverse Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "34",
                            lvis#"EndorsementName": "[ALTA 15-06 ] Non-Imputation - Full Equity Transfer",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1767",
                            lvis#"EndorsementName": "[ALTA 15.1-06] Nonimputation – Additional Insured",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "36",
                            lvis#"EndorsementName": "[ALTA 15.2-06 ] Non-Imputation - Partial Equity Transfer",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "38",
                            lvis#"EndorsementName": "[ALTA 17-06 ] Access and Entry",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "39",
                            lvis#"EndorsementName": "[ALTA 17.1-06 ] Indirect Access and Entry",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7396",
                            lvis#"EndorsementName": "[ALTA 17.2-06 ] Utility Access",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "40",
                            lvis#"EndorsementName": "[ALTA 18-06 ] Single Tax Parcel",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "41",
                            lvis#"EndorsementName": "[ALTA 18.1-06] Multiple Tax Parcel - Easements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "42",
                            lvis#"EndorsementName": "[ALTA 19-06 ] Contiguity - Multiple Parcels",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "43",
                            lvis#"EndorsementName": "[ALTA 19.1-06 ] Contiguity - Single Parcel",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8403",
                            lvis#"EndorsementName": "[ALTA 19.2-06] Contiguity - Specified Parcels",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "44",
                            lvis#"EndorsementName": "[ALTA 20-06 ] First Loss - Multiple Parcel Transactions",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "45",
                            lvis#"EndorsementName": "[ALTA 22-06] Location",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "46",
                            lvis#"EndorsementName": "[ALTA 22.1-06 ] Location and Map",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7398",
                            lvis#"EndorsementName": "[ALTA 24-06 ] Doing Business",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7399",
                            lvis#"EndorsementName": "[ALTA 25-06 ] Same as Survey",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7400",
                            lvis#"EndorsementName": "[ALTA 25.1-06 ] Same as Portion of Survey",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7401",
                            lvis#"EndorsementName": "[ALTA 26-06 ] Subdivision",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7402",
                            lvis#"EndorsementName": "[ALTA 27-06 ] Usury",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7403",
                            lvis#"EndorsementName": "[ALTA 28-06 ] Easement - Damage or Enforced Removal",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8084",
                            lvis#"EndorsementName": "[ALTA 28.1-06] Encroachments - Boundaries and Easements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8258",
                            lvis#"EndorsementName": "[ALTA 28.2-06] Encroachments - Boundaries and Easements - Described Improvements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8404",
                            lvis#"EndorsementName": "[ALTA 28.3-06] Encroachments - Boundaries and Easements - Land Under Development",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7869",
                            lvis#"EndorsementName": "[ALTA 30-06] Shared Appreciation Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7873",
                            lvis#"EndorsementName": "[ALTA 31-06] Severable Improvements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8042",
                            lvis#"EndorsementName": "[ALTA 32-06] Construction Loan",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8043",
                            lvis#"EndorsementName": "[ALTA 32.1-06] Construction Loan - Direct Payment",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8149",
                            lvis#"EndorsementName": "[ALTA 32.2-06] Construction Loan - Insured's Direct Payment",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8044",
                            lvis#"EndorsementName": "[ALTA 33-06] Disbursement",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8066",
                            lvis#"EndorsementName": "[ALTA 34-06] Identified Risk Coverage",
                            lvis#"EffectiveDate": "2016-06-01",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "-1"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8103",
                            lvis#"EndorsementName": "[ALTA 35-06] Minerals And Other Subsurface Substances - Buildings",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8104",
                            lvis#"EndorsementName": "[ALTA 35.1-06] Minerals And Other Subsurface Substances - Improvements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8105",
                            lvis#"EndorsementName": "[ALTA 35.2-06] Minerals And Other Subsurface Substances - Described Improvements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8106",
                            lvis#"EndorsementName": "[ALTA 35.3-06] Minerals And Other Subsurface Substances - Land Under Development",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8107",
                            lvis#"EndorsementName": "[ALTA 36-06] Energy Project - Leasehold/Easement - Owner’s Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8108",
                            lvis#"EndorsementName": "[ALTA 36.1-06] Energy Project - Leasehold/Easement - Loan Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8109",
                            lvis#"EndorsementName": "[ALTA 36.2-06] Energy Project - Leasehold - Owner’s Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8110",
                            lvis#"EndorsementName": "[ALTA 36.3-06] Energy Project - Leasehold - Loan Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8111",
                            lvis#"EndorsementName": "[ALTA 36.4-06] Energy Project - Covenants, Conditions and Restrictions - Land Under Dev. - Owner’s Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8112",
                            lvis#"EndorsementName": "[ALTA 36.5-06] Energy Project - Covenants, Conditions and Restrictions - Land Under Dev. - Loan",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8113",
                            lvis#"EndorsementName": "[ALTA 36.6-06] Energy Project - Encroachments",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8352",
                            lvis#"EndorsementName": "[ALTA 36.7-06] Energy Project - Fee Estate - Owner’s Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8353",
                            lvis#"EndorsementName": "[ALTA 36.8-06] Energy Project - Fee Estate - Loan Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8247",
                            lvis#"EndorsementName": "[ALTA 37-06] Assignment of Rents or Leases",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8260",
                            lvis#"EndorsementName": "[ALTA 38-06] Mortgage Tax",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8261",
                            lvis#"EndorsementName": "[ALTA 39-06] Policy Authentication",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8286",
                            lvis#"EndorsementName": "[ALTA 41-06] Water - Buildings",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8287",
                            lvis#"EndorsementName": "[ALTA 41.1-06] Water - Improvements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8288",
                            lvis#"EndorsementName": "[ALTA 41.2-06] Water - Described Improvements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8289",
                            lvis#"EndorsementName": "[ALTA 41.3-06] Water - Land Under Development",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8291",
                            lvis#"EndorsementName": "[ALTA 43-06] Anti-Taint - Loan Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8292",
                            lvis#"EndorsementName": "[ALTA 44-06] Insured Mortgage Recording - Loan",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8354",
                            lvis#"EndorsementName": "[ALTA 45-06] Pari Passu Mortgage - Loan Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "483",
                            lvis#"EndorsementName": "[AZ - NFE] Non Filed Endorsements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "-1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "-1"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "108",
                            lvis#"EndorsementName": "[CLTA 100-06] Restrictions, Encroachments & Minerals",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "109",
                            lvis#"EndorsementName": "[CLTA 100.1] Restrictions, Encroachments & Minerals - Lender",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "110",
                            lvis#"EndorsementName": "[CLTA 100.2-06] Restrictions, Encroachments, Minerals",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8085",
                            lvis#"EndorsementName": "[CLTA 100.2.1-06] Covenants, Conditions and Restrictions - Loan Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "111",
                            lvis#"EndorsementName": "[CLTA 100.4-06 ] CC&R's, Violations",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "112",
                            lvis#"EndorsementName": "[CLTA 100.5-06 ] CC&R's, Violations",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "113",
                            lvis#"EndorsementName": "[CLTA 100.6-06] CC&R's, Including Future Violations - Owner's Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "114",
                            lvis#"EndorsementName": "[CLTA 100.7-06 ] CC&R's, Violations",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "115",
                            lvis#"EndorsementName": "[CLTA 100.8-06 ] CC&R's, Violations",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8088",
                            lvis#"EndorsementName": "[CLTA 100.9-06] Covenants, Conditions and Restrictions - Unimproved Land - Owner's Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8089",
                            lvis#"EndorsementName": "[CLTA 100.10-06] Covenants, Conditions and Restrictions - Improved Land - Owner's Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "116",
                            lvis#"EndorsementName": "[CLTA 100.12-06] CC&R's, Right of Reversion",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "117",
                            lvis#"EndorsementName": "[CLTA 100.13-06] CC&R's, Assessment Liens - ALTA Lender",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "-1",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "118",
                            lvis#"EndorsementName": "[CLTA 100.17-06] CC&R's, Proper Modification",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "119",
                            lvis#"EndorsementName": "[CLTA 100.18-06] CC&R's, Right of Reversion",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "120",
                            lvis#"EndorsementName": "[CLTA 100.19-06] CC&R's, Violations",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "121",
                            lvis#"EndorsementName": "[CLTA 100.20-06] CC&R's, Violations",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "122",
                            lvis#"EndorsementName": "[CLTA 100.21-06] CC&R's, Plans and Specifications",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "123",
                            lvis#"EndorsementName": "[CLTA 100.23-06] Minerals, Surface Damage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "124",
                            lvis#"EndorsementName": "[CLTA 100.24-06] Minerals, Surface Entry by Damage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "125",
                            lvis#"EndorsementName": "[CLTA 100.25-06] Minerals, Surface Use",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "126",
                            lvis#"EndorsementName": "[CLTA 100.26-06] Minerals, Present-Future Improvements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "127",
                            lvis#"EndorsementName": "[CLTA 100.27-06] CC&R's, Violations",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "128",
                            lvis#"EndorsementName": "[CLTA 100.28-06] CC&R's, Violation - Future Improvements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7842",
                            lvis#"EndorsementName": "[CLTA 100.29-06] Minerals, Surface Damage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "130",
                            lvis#"EndorsementName": "[CLTA 101] Mechanic's Liens - CLTA Lender",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "131",
                            lvis#"EndorsementName": "[CLTA 101.1-06] Mechanics' Liens",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "132",
                            lvis#"EndorsementName": "[CLTA 101.2-06] Mechanics' Liens, Notice of Completion",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "133",
                            lvis#"EndorsementName": "[CLTA 101.3-06] Mechanics' Liens, No Notice of Completion",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "134",
                            lvis#"EndorsementName": "[CLTA 101.4] Mechanics' Liens, No Notice of Completion",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "135",
                            lvis#"EndorsementName": "[CLTA 101.5-06] Mechanics' Liens, Notice of Completion",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "136",
                            lvis#"EndorsementName": "[CLTA 101.6-06] Mechanics' Liens, Notice of Completion",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "137",
                            lvis#"EndorsementName": "[CLTA 101.8] Mechanic's Liens - Lender",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "138",
                            lvis#"EndorsementName": "[CLTA 101.9-06] Mechanics' Liens, Notice of Completion",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "139",
                            lvis#"EndorsementName": "[CLTA 101.10-06] Mechanics' Liens, Notice of Completion",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "140",
                            lvis#"EndorsementName": "[CLTA 101.11-06] Mechanics' Liens, No Notice of Completion",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "141",
                            lvis#"EndorsementName": "[CLTA 101.12-06] Mechanics' Liens, No Notice of Completion",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "142",
                            lvis#"EndorsementName": "[CLTA 101.13-06] Mechanics' Liens, Notice of Completion",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "143",
                            lvis#"EndorsementName": "[CLTA 102.4-06] Foundation",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "144",
                            lvis#"EndorsementName": "[CLTA 102.5-06] Foundation",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "145",
                            lvis#"EndorsementName": "[CLTA 102.6-06] Foundation, Portion of Premises",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "146",
                            lvis#"EndorsementName": "[CLTA 102.7-06] Foundation, Portion of Premises",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "147",
                            lvis#"EndorsementName": "[CLTA 103.1-06] Easement, Damage or Enforced Removal",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8071",
                            lvis#"EndorsementName": "[CLTA 103.2-06] Easement, Damage - Use or Maintenance",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "148",
                            lvis#"EndorsementName": "[CLTA 103.3-06] Easement, Existing Encroachment, Enforced Removal",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "149",
                            lvis#"EndorsementName": "[CLTA 103.4-06] Easement, Access to Public Street",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "150",
                            lvis#"EndorsementName": "[CLTA 103.5-06] Water Rights, Surface Damage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "151",
                            lvis#"EndorsementName": "[CLTA 103.6-06] Encroachments, None Exist",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "4125",
                            lvis#"EndorsementName": "[CLTA 103.7-06] Land Abuts Street",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "152",
                            lvis#"EndorsementName": "[CLTA 103.8-06] Water Rights, Future Improvements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "153",
                            lvis#"EndorsementName": "[CLTA 103.9-06] Encroachment, Future Improvements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "154",
                            lvis#"EndorsementName": "[CLTA 103.10-06] Surface Use, Horizontal Subdivision",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8067",
                            lvis#"EndorsementName": "[CLTA 103.11-06] Access and Entry",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8079",
                            lvis#"EndorsementName": "[CLTA 103.12-06] Indirect Access and Entry",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "802",
                            lvis#"EndorsementName": "[CLTA 104] Assignment of Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "156",
                            lvis#"EndorsementName": "[CLTA 104.A] Assignment of Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "689",
                            lvis#"EndorsementName": "[CLTA 104.1] Assignment of Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "158",
                            lvis#"EndorsementName": "[CLTA 104.4-06] Collateral Assignment of Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "159",
                            lvis#"EndorsementName": "[CLTA 104.6-06] Assignment of Rents or Leases",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "160",
                            lvis#"EndorsementName": "[CLTA 104.7-06] Assignment of Rents/Leases",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "161",
                            lvis#"EndorsementName": "[CLTA 104.8-06 ] Assignment of Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "162",
                            lvis#"EndorsementName": "[CLTA 104.9] Assignment of Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "163",
                            lvis#"EndorsementName": "[CLTA 104.10-06 ] Assignment of Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "164",
                            lvis#"EndorsementName": "[CLTA 104.11-06 ] Collateral Assignment of Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "165",
                            lvis#"EndorsementName": "[CLTA 104.12-06] Assignment",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "166",
                            lvis#"EndorsementName": "[CLTA 104.13-06 ] Assignment and Date Down",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "167",
                            lvis#"EndorsementName": "[CLTA 105-06] Multiple Mortgages in One Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "175",
                            lvis#"EndorsementName": "[CLTA 107.1-06] Allocation of Liability of Parcels",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "176",
                            lvis#"EndorsementName": "[CLTA 107.2-06 ] Amount of Insurance, Increase",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "177",
                            lvis#"EndorsementName": "[CLTA 107.5-06] Leasehold Improvements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "178",
                            lvis#"EndorsementName": "[CLTA 107.9-06 ] Additional Insured",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "179",
                            lvis#"EndorsementName": "[CLTA 107.10-06 ] Additional Insured",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "180",
                            lvis#"EndorsementName": "[CLTA 107.11-06] Non-Merger After Lender Acquires Title",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "184",
                            lvis#"EndorsementName": "[CLTA 108.10-06] Revolving Credit Loan, Increase",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "185",
                            lvis#"EndorsementName": "[CLTA 109 ] Oil and Gas Lease, No Assignments",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "186",
                            lvis#"EndorsementName": "[CLTA 110.1-06] Deletion of Item From Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "187",
                            lvis#"EndorsementName": "[CLTA 110.3-06] Minerals, Conveyance of Surface Rights",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "753",
                            lvis#"EndorsementName": "[CLTA 110.4] Modification of Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "693",
                            lvis#"EndorsementName": "[CLTA 110.5] Modification of Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "190",
                            lvis#"EndorsementName": "[CLTA 110.6] Modification of Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "191",
                            lvis#"EndorsementName": "[CLTA 110.7-06] Insurance Against Enforceability of Item",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "192",
                            lvis#"EndorsementName": "[CLTA 110.9-06] Environmental Protection Lien",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "193",
                            lvis#"EndorsementName": "[CLTA 110.10-06 ] Modification and Additional Advance",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8091",
                            lvis#"EndorsementName": "[CLTA 110.11-06] Mortgage Modification",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8068",
                            lvis#"EndorsementName": "[CLTA 110.11.1-06] Mortgage Modification with Subordination",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "194",
                            lvis#"EndorsementName": "[CLTA 111-06] Mortgage Priority, Partial Reconveyance",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "195",
                            lvis#"EndorsementName": "[CLTA 111.1-06] Mortgage Priority, Partial Reconveyance",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "196",
                            lvis#"EndorsementName": "[CLTA 111.2-06] Mortgage Priority, Subordination",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "197",
                            lvis#"EndorsementName": "[CLTA 111.3-06] Mortgage Priority, Encroachment, Address",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "198",
                            lvis#"EndorsementName": "[CLTA 111.4-06] Mortgage Impairment After Conveyance",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "199",
                            lvis#"EndorsementName": "[CLTA 111.5-06] Variable Rate Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "201",
                            lvis#"EndorsementName": "[CLTA 111.7-06] Variable Rate, Renewal",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "202",
                            lvis#"EndorsementName": "[CLTA 111.8-06] Variable Rate Mortgage - Negative Amortization",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "203",
                            lvis#"EndorsementName": "[CLTA 111.9-06] Variable Rate, FNMA 7 Year Balloon",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "204",
                            lvis#"EndorsementName": "[CLTA 111.10-06] Revolving Credit Loan, Optional Advance",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "205",
                            lvis#"EndorsementName": "[CLTA 111.11-06] Revolving Credit Loan, Obligatory Advance",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7367",
                            lvis#"EndorsementName": "[CLTA 111.14] Future Advance - Priority",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7369",
                            lvis#"EndorsementName": "[CLTA 111.14.1 ] Future Advance - Knowledge",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7371",
                            lvis#"EndorsementName": "[CLTA 111.14.2 ] Future Advance - Letter of Credit",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8541",
                            lvis#"EndorsementName": "[CLTA 111.14.3] Future Advance – Reverse Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8486",
                            lvis#"EndorsementName": "[CLTA 112.1B-06] Revolving Credit Loan, Obligatory Advance",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "209",
                            lvis#"EndorsementName": "[CLTA 114-06] Co-insurance",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "210",
                            lvis#"EndorsementName": "[CLTA 114.1-06] Co-Insurance, Joint and Several Liability",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "211",
                            lvis#"EndorsementName": "[CLTA 114.2-06] Co-Insurance, Joint and Several Liability",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "212",
                            lvis#"EndorsementName": "[CLTA 115-06] Condominium",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2564",
                            lvis#"EndorsementName": "[CLTA 115.1] Condominium",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "213",
                            lvis#"EndorsementName": "[CLTA 115.1-06] Condominium - Assessments Priority",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "-1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2565",
                            lvis#"EndorsementName": "[CLTA 115.2] Planned Unit Development",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7894",
                            lvis#"EndorsementName": "[CLTA 115.3] Condominium",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7378",
                            lvis#"EndorsementName": "[CLTA 115.3-06] Condominium - Current Assessments",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "215",
                            lvis#"EndorsementName": "[CLTA 116-06] Designation of Improvements, Address",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "216",
                            lvis#"EndorsementName": "[CLTA 116.1-06] Same as Survey",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7379",
                            lvis#"EndorsementName": "[CLTA 116.01-06 ] Location",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8092",
                            lvis#"EndorsementName": "[CLTA 116.1.2-06] Same as Portion of Survey",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7380",
                            lvis#"EndorsementName": "[CLTA 116.02-06 ] Location and Map",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "217",
                            lvis#"EndorsementName": "[CLTA 116.2-06] Designation of Improvements, Condominium",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "218",
                            lvis#"EndorsementName": "[CLTA 116.3-06] Legal Description, New Subdivision",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "219",
                            lvis#"EndorsementName": "[CLTA 116.4-06] Contiguity, Single Parcel",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "220",
                            lvis#"EndorsementName": "[CLTA 116.4.1-06] Contiguity, Multiple Parcels",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "221",
                            lvis#"EndorsementName": "[CLTA 116.5-06] Manufactured Housing Unit",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "222",
                            lvis#"EndorsementName": "[CLTA 116.5.1-06] Manufactured Housing - Conversion; Loan",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "223",
                            lvis#"EndorsementName": "[CLTA 116.5.2-06] Manufactured Housing - Conversion; Owners",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "224",
                            lvis#"EndorsementName": "[CLTA 116.6-06] Manufactured Housing Unit",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "225",
                            lvis#"EndorsementName": "[CLTA 116.7-06] Subdivision Map Act Compliance",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8080",
                            lvis#"EndorsementName": "[CLTA 116.8-06] Subdivision",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "226",
                            lvis#"EndorsementName": "[CLTA 119-06] Validity of Lease in Schedule B",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2575",
                            lvis#"EndorsementName": "[CLTA 119.1] Leasehold Policy, Additional Exceptions",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "228",
                            lvis#"EndorsementName": "[CLTA 119.2-06] Validity and Priority of Lease",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "229",
                            lvis#"EndorsementName": "[CLTA 119.3-06] Priority of Lease",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2578",
                            lvis#"EndorsementName": "[CLTA 119.4] Validity of Sublease, Joint Powers",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "4196",
                            lvis#"EndorsementName": "[CLTA 119.5-06] Leasehold - Owner",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7385",
                            lvis#"EndorsementName": "[CLTA 119.6-06] Leasehold - Loan",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1715",
                            lvis#"EndorsementName": "[CLTA 120.2] DOT Subordinate to Lease",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "232",
                            lvis#"EndorsementName": "[CLTA 122-06] Construction Lender Advance",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8487",
                            lvis#"EndorsementName": "[CLTA 122.1A-06] Construction Loan Advance-Initial Advance-2006",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "233",
                            lvis#"EndorsementName": "[CLTA 122.2-06] Construction Lender Advance",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1386",
                            lvis#"EndorsementName": "[CLTA 123.1] Zoning - Unimproved Land",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "234",
                            lvis#"EndorsementName": "[CLTA 123.1-06] Zoning, Unimproved Land",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1388",
                            lvis#"EndorsementName": "[CLTA 123.2] Zoning - Improved Land",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "235",
                            lvis#"EndorsementName": "[CLTA 123.2-06] Zoning, Completed Structure",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8369",
                            lvis#"EndorsementName": "[CLTA 123.3-06] Zoning - Land Under Development",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "236",
                            lvis#"EndorsementName": "[CLTA 124.1-06] Covenants are Binding",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "237",
                            lvis#"EndorsementName": "[CLTA 124.2-06] Covenants in Lease are Binding",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "238",
                            lvis#"EndorsementName": "[CLTA 124.3-06] Covenants in Lease are Binding",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8093",
                            lvis#"EndorsementName": "[CLTA 127-06] Nonimputation - Partial Equity Transfer",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8094",
                            lvis#"EndorsementName": "[CLTA 127.1-06] Nonimputation - Full Equity Transfer",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8095",
                            lvis#"EndorsementName": "[CLTA 127.2-06] Nonimputation - Additional Insured",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8096",
                            lvis#"EndorsementName": "[CLTA 129-06] Single Tax Parcel",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "7393",
                            lvis#"EndorsementName": "[CLTA 129.1-06] Multiple Tax Parcel - Easements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8097",
                            lvis#"EndorsementName": "[CLTA 130-06] First Loss, Multiple Parcel Transactions",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "246",
                            lvis#"EndorsementName": "[CLTA 132-06] Usury",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8098",
                            lvis#"EndorsementName": "[CLTA 133-06] Doing Business",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8101",
                            lvis#"EndorsementName": "[CLTA 135-06] One to Four Family Shared Appreciation",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8373",
                            lvis#"EndorsementName": "[CLTA 136-06] Severable Improvements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8374",
                            lvis#"EndorsementName": "[CLTA 137-06] Construction Loan - Loss of Priority",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8375",
                            lvis#"EndorsementName": "[CLTA 137.1-06] Construction Loan - Loss of Priority - Direct Payment",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "-1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "-1",
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8376",
                            lvis#"EndorsementName": "[CLTA 137.2-06] Construction Loan - Loss of Priority - Insured's Direct Payment",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8377",
                            lvis#"EndorsementName": "[CLTA 138-06] Disbursement",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8378",
                            lvis#"EndorsementName": "[CLTA 140-06] Minerals And Other Subsurface Substances - Buildings",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8379",
                            lvis#"EndorsementName": "[CLTA 140.1-06] Minerals And Other Subsurface Substances - Improvements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8380",
                            lvis#"EndorsementName": "[CLTA 140.2-06] Minerals And Other Subsurface Substances - Described Improvements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8381",
                            lvis#"EndorsementName": "[CLTA 140.3-06] Minerals And Other Subsurface Substances - Land Under Development",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8382",
                            lvis#"EndorsementName": "[CLTA 141-06] Energy Project - Leasehold/ Easement - Owner’s",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8383",
                            lvis#"EndorsementName": "[CLTA 141.1-06] Energy Project - Leasehold/ Easement - Loan",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8384",
                            lvis#"EndorsementName": "[CLTA 141.2-06] Energy Project - Leasehold - Owner’s",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8385",
                            lvis#"EndorsementName": "[CLTA 141.3-06] Energy Project - Leasehold - Loan",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8386",
                            lvis#"EndorsementName": "[CLTA 141.4-06] Energy Project - Covenants, Conditions and Restrictions - Land Under Dev. - Owner’s",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8387",
                            lvis#"EndorsementName": "[CLTA 141.5-06] Energy Project - Covenants, Conditions and Restrictions - Land Under Dev. - Loan",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8388",
                            lvis#"EndorsementName": "[CLTA 141.6-06] Energy Project - Encroachments",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8389",
                            lvis#"EndorsementName": "[CLTA 142-06] Policy Authentication",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8390",
                            lvis#"EndorsementName": "[CLTA 143-06] Water-Buildings",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8391",
                            lvis#"EndorsementName": "[CLTA 143.1-06] Water-Improvements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8392",
                            lvis#"EndorsementName": "[CLTA 143.2-06] Water-Described Improvements",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8393",
                            lvis#"EndorsementName": "[CLTA 143.3-06] Water-Land Under Development",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8395",
                            lvis#"EndorsementName": "[CLTA 145-06] Anti-Taint",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8396",
                            lvis#"EndorsementName": "[CLTA 146-06] Insured Mortgage Recording-Loan",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "49",
                            lvis#"EndorsementName": "[FA 1] Policy Correction - Deletion/Paragraph/Substitution",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "-1"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "50",
                            lvis#"EndorsementName": "[FA 2] Encroachment onto Easement - DVA",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "51",
                            lvis#"EndorsementName": "[FA 3] Minerals - Surface Damage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "52",
                            lvis#"EndorsementName": "[FA 3.A] Minerals - Surface Damage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "53",
                            lvis#"EndorsementName": "[FA 4] Reservation, Use or Maintenance",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1357",
                            lvis#"EndorsementName": "[FA 5] Modification of Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "340",
                            lvis#"EndorsementName": "[FA 5.1 ] Future Modification of Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "54",
                            lvis#"EndorsementName": "[FA 6] No Improvements, Partial Reconveyance",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "56",
                            lvis#"EndorsementName": "[FA 8] Zoning",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2042",
                            lvis#"EndorsementName": "[FA 10] Assignment of Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "57",
                            lvis#"EndorsementName": "[FA 13] Manufactured Housing Unit",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "60",
                            lvis#"EndorsementName": "[FA 14] Leasehold Policy Conversion",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "61",
                            lvis#"EndorsementName": "[FA 15] Leasehold Policy Conversion",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "62",
                            lvis#"EndorsementName": "[FA 15.1 ] Leasehold Policy Conversion",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "63",
                            lvis#"EndorsementName": "[FA 16] Leasehold Policy Conversion",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "64",
                            lvis#"EndorsementName": "[FA 16.1 ] Leasehold Policy Conversion",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "65",
                            lvis#"EndorsementName": "[FA 20] Shared Appreciation Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2057",
                            lvis#"EndorsementName": "[FA 21.1 ] Revolving Credit Loan",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "67",
                            lvis#"EndorsementName": "[FA 22.1 ] Revolving Credit Loan",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "68",
                            lvis#"EndorsementName": "[FA 22.1A] Revolving Credit Loan",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "69",
                            lvis#"EndorsementName": "[FA 23] Revolving Credit Loan",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "70",
                            lvis#"EndorsementName": "[FA 25] Revolving Credit Loan",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "71",
                            lvis#"EndorsementName": "[FA 25.A ] Revolving Credit Loan",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "72",
                            lvis#"EndorsementName": "[FA 26] Revolving Credit Loan - Optional Advance",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2067",
                            lvis#"EndorsementName": "[FA 27] Environmental Lien, 1984 Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2068",
                            lvis#"EndorsementName": "[FA 27.1 ] Exclusions From Coverage, 1984 Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "73",
                            lvis#"EndorsementName": "[FA 28] Revolving Credit Loan",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "74",
                            lvis#"EndorsementName": "[FA 28.A ] Revolving Credit Loan",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "84",
                            lvis#"EndorsementName": "[FA 29] Revolving Credit Loan - Increase",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "85",
                            lvis#"EndorsementName": "[FA 29.1] Revolving Credit Loan - Increase",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "86",
                            lvis#"EndorsementName": "[FA 30.1 ] Leasehold Policy Conversion",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "87",
                            lvis#"EndorsementName": "[FA 31] Restrictions, Easements, Minerals - Unimproved Land",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1798",
                            lvis#"EndorsementName": "[FA 31.1] REM Unimproved",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "89",
                            lvis#"EndorsementName": "[FA 31.2] Restrictions, Easements, Minerals - Unimproved Land, Owner",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "90",
                            lvis#"EndorsementName": "[FA 32] Land Same as Map Attached to Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1254",
                            lvis#"EndorsementName": "[FA 32.1 ] Designation of Improvements, Address",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "91",
                            lvis#"EndorsementName": "[FA 33] Truth in Lending - ALTA Policies",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "92",
                            lvis#"EndorsementName": "[FA 35] Environmental Protection Lien",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "93",
                            lvis#"EndorsementName": "[FA 36] Variable Rate - Negative Amortization",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "94",
                            lvis#"EndorsementName": "[FA 36.1] Variable Rate FNMA 7 Year Balloon",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "95",
                            lvis#"EndorsementName": "[FA 37] Co-Insurance Clause - No Subsequent Improvement",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "96",
                            lvis#"EndorsementName": "[FA 38] Arbitration Clause, Modification",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "97",
                            lvis#"EndorsementName": "[FA 38.1 ] Arbitration Clause, Modification",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "98",
                            lvis#"EndorsementName": "[FA 39] Arbitration Clause, Modification",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1156",
                            lvis#"EndorsementName": "[FA 41] Assignment of Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2105",
                            lvis#"EndorsementName": "[FA 42] Restrictions, Encroachments & Minerals",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "690",
                            lvis#"EndorsementName": "[FA 43] Easements in Declaration",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "691",
                            lvis#"EndorsementName": "[FA 43.1 ] Leasehold - Interference in Use",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "696",
                            lvis#"EndorsementName": "[FA 43.2 ] Leasehold - Interference in Use",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "100",
                            lvis#"EndorsementName": "[FA 46] Express Policy, Assignment of Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "101",
                            lvis#"EndorsementName": "[FA 47] Modification of Mortgage, Limited",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "102",
                            lvis#"EndorsementName": "[FA 48] Modification of Mortgage, Limited",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "103",
                            lvis#"EndorsementName": "[FA 48.1 ] Modification of Mortgage, Limited",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1919",
                            lvis#"EndorsementName": "[FA 50.1] First Loss",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "338",
                            lvis#"EndorsementName": "[FA 50.2 ] First Loss Payable",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1920",
                            lvis#"EndorsementName": "[FA 50.3] First Loss",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "339",
                            lvis#"EndorsementName": "[FA 51 ] Last Dollar",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2124",
                            lvis#"EndorsementName": "[FA 51.1 ] Last Dollar",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2125",
                            lvis#"EndorsementName": "[FA 51.2 ] Last Dollar",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1922",
                            lvis#"EndorsementName": "[FA 52] Non-Imputation",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1923",
                            lvis#"EndorsementName": "[FA 52.1] Non-Imputation/Partnership",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2128",
                            lvis#"EndorsementName": "[FA 53] Litigation Guarantee Amendment",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2129",
                            lvis#"EndorsementName": "[FA 54] Litigation Guarantee Amendment",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "343",
                            lvis#"EndorsementName": "[FA 55] Fairway",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "706",
                            lvis#"EndorsementName": "[FA 55.1 ] Fairway and Successor Insured",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "344",
                            lvis#"EndorsementName": "[FA 56] Reverse Mortgage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1372",
                            lvis#"EndorsementName": "[FA 57] Usury I",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1373",
                            lvis#"EndorsementName": "[FA 57.1 ] Usury II",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "345",
                            lvis#"EndorsementName": "[FA 58] Doing Business - Lender",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2136",
                            lvis#"EndorsementName": "[FA 59] Anti-Taint",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2138",
                            lvis#"EndorsementName": "[FA 60] Recharacterization - Lender Only",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1021",
                            lvis#"EndorsementName": "[FA 61] Construction Loan Pending Disbursement",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1022",
                            lvis#"EndorsementName": "[FA 61.1 ] Construction Loan Disbursement",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1023",
                            lvis#"EndorsementName": "[FA 61.2] F.A. Form 61.2 - Construction Loan - Reinstatement of Covered Risk 11 (a)",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2142",
                            lvis#"EndorsementName": "[FA 61.3 ] Pending Improvements - Owner",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2143",
                            lvis#"EndorsementName": "[FA 62] Pending Disbursement - 122",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "5935",
                            lvis#"EndorsementName": "[FA 63] Modification of DOT with Creditor's Rights",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2145",
                            lvis#"EndorsementName": "[FA 63.1 ] Mod. of Mortgage - Cred. Rights Excl.",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2146",
                            lvis#"EndorsementName": "[FA 63.2 ] Mod. of Mortgage - Cred. Rights Excl.",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "772",
                            lvis#"EndorsementName": "[FA 73] Tenant Umbrella Coverage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1375",
                            lvis#"EndorsementName": "[FA 74] Zoning",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "1376",
                            lvis#"EndorsementName": "[FA 75] Zoning - Completed Structure",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "2160",
                            lvis#"EndorsementName": "[FA 76] Expanded Survey Coverage",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "107",
                            lvis#"EndorsementName": "[FA 88] Reverse Mortgage - (Trust Mortgagor)",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8045",
                            lvis#"EndorsementName": "[FA 92] Deletion of Natural Person Limitation",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "47",
                            lvis#"EndorsementName": "[JR1] ALTA JR 1",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "48",
                            lvis#"EndorsementName": "[JR2] JR2 Future Advance",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "454",
                            lvis#"EndorsementName": "[LTAA 1] Assignment of Mortgage or Deed of Trust Endorsement(CLTA 104)",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "455",
                            lvis#"EndorsementName": "[LTAA 1.1] Assignment of Mortgage or Deed of Trust Endorsement",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "456",
                            lvis#"EndorsementName": "[LTAA 2] Additional Advance Endorsement",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "457",
                            lvis#"EndorsementName": "[LTAA 3] Restrictions, Encroachments & Minerals: ALTA Extended Lender",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8538",
                            lvis#"EndorsementName": "[LTAA 3R Mod] Restrictions, Encroachments & Minerals: ALTA Lender",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8901",
                            lvis#"EndorsementName": "[LTAA 3R] Restrictions, Encroachments & Minerals: ALTA Extended Lender",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "458",
                            lvis#"EndorsementName": "[LTAA 4] Assignment of Mortgage or Deed of Trust Endorsement (FNMA)",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "459",
                            lvis#"EndorsementName": "[LTAA 5] Type of Improvement (CLTA 116)",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8484",
                            lvis#"EndorsementName": "[LTAA 6] Variable Rate Mortgage, Regulation",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "460",
                            lvis#"EndorsementName": "[LTAA 7] Modification of Policy",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "-1"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "461",
                            lvis#"EndorsementName": "[LTAA 9] Partial Release (CLTA 111)",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "463",
                            lvis#"EndorsementName": "[LTAA 10] Lender's Bringdown (No Liability Increase) (CLTA 110.5)",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "464",
                            lvis#"EndorsementName": "[LTAA 10.1] Lender's Bringdown (No Liability Increase) (CLTA 110.5)",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "465",
                            lvis#"EndorsementName": "[LTAA 12] Specific Subordination",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8481",
                            lvis#"EndorsementName": "[LTAA 14] Street Assessments",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "466",
                            lvis#"EndorsementName": "[LTAA 15] Trustee Sale Guarantee Update",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "4"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "-1"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8482",
                            lvis#"EndorsementName": "[LTAA 16] Zoning, Unimproved Land",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8483",
                            lvis#"EndorsementName": "[LTAA 17] Zoning, Improved Land",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "6165",
                            lvis#"EndorsementName": "[LTAA 21]  Bringdown (Property Added/No Liability Increase)",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "1",
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "8485",
                            lvis#"EndorsementName": "[LTAA 23] Foundation",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        },
                        lvis#"Endorsement": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"EndorsementId": "480",
                            lvis#"EndorsementName": "[LTAA 27] Collateral Assignment",
                            lvis#"EffectiveDate": "2017-08-05",
                            lvis#"ValidPolicyCategoryIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2"
                              }
                            },
                            lvis#"ValidPolicyCoverageIds": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"string": "2",
                                lvis#"string": "3"
                              }
                            },
                            lvis#"concurrent_Endorsements": null,
                            lvis#"excluded_Endorsements": null
                          }
                        }
                      }
                    },
                    lvis#"MaxNumberOfPolciesAllowed": "2",
                    lvis#"Notes": null,
                    lvis#"PolicyProducts": do {
                      ns lvis http://services.firstam.com/lvis/v2.0
                      ---
                      {
                        lvis#"PolicyProduct": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"DefaultRateTypeId": "0",
                            lvis#"EffectiveDate": "2000-01-01",
                            lvis#"IsDefault": "false",
                            lvis#"PolicyCategoryId": "2",
                            lvis#"PolicyCoverageId": "1",
                            lvis#"PolicyCoverageName": "Standard",
                            lvis#"PolicyId": "469",
                            lvis#"PolicyName": "ALTA Loan Policy",
                            lvis#"ValidRateTypes": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "1",
                                    lvis#"Value": "Basic"
                                  }
                                },
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "35",
                                    lvis#"Value": "Investor Rate"
                                  }
                                }
                              }
                            }
                          }
                        },
                        lvis#"PolicyProduct": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"DefaultRateTypeId": "0",
                            lvis#"EffectiveDate": "2000-01-01",
                            lvis#"IsDefault": "false",
                            lvis#"PolicyCategoryId": "2",
                            lvis#"PolicyCoverageId": "2",
                            lvis#"PolicyCoverageName": "Extended",
                            lvis#"PolicyId": "470",
                            lvis#"PolicyName": "ALTA Loan Policy - Extended",
                            lvis#"ValidRateTypes": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "1",
                                    lvis#"Value": "Basic"
                                  }
                                },
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "35",
                                    lvis#"Value": "Investor Rate"
                                  }
                                }
                              }
                            }
                          }
                        },
                        lvis#"PolicyProduct": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"DefaultRateTypeId": "0",
                            lvis#"EffectiveDate": "2000-01-01",
                            lvis#"IsDefault": "false",
                            lvis#"PolicyCategoryId": "1",
                            lvis#"PolicyCoverageId": "1",
                            lvis#"PolicyCoverageName": "Standard",
                            lvis#"PolicyId": "471",
                            lvis#"PolicyName": "ALTA Owner's Policy",
                            lvis#"ValidRateTypes": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "1",
                                    lvis#"Value": "Basic"
                                  }
                                },
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "35",
                                    lvis#"Value": "Investor Rate"
                                  }
                                },
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "281",
                                    lvis#"Value": "Short Term Residential Non-Prepaid"
                                  }
                                },
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "282",
                                    lvis#"Value": "Short Term Residential Prepaid"
                                  }
                                }
                              }
                            }
                          }
                        },
                        lvis#"PolicyProduct": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"DefaultRateTypeId": "0",
                            lvis#"EffectiveDate": "2000-01-01",
                            lvis#"IsDefault": "false",
                            lvis#"PolicyCategoryId": "1",
                            lvis#"PolicyCoverageId": "2",
                            lvis#"PolicyCoverageName": "Extended",
                            lvis#"PolicyId": "472",
                            lvis#"PolicyName": "ALTA Owner's Policy - Extended",
                            lvis#"ValidRateTypes": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "1",
                                    lvis#"Value": "Basic"
                                  }
                                },
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "35",
                                    lvis#"Value": "Investor Rate"
                                  }
                                },
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "281",
                                    lvis#"Value": "Short Term Residential Non-Prepaid"
                                  }
                                },
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "282",
                                    lvis#"Value": "Short Term Residential Prepaid"
                                  }
                                }
                              }
                            }
                          }
                        },
                        lvis#"PolicyProduct": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"DefaultRateTypeId": "0",
                            lvis#"EffectiveDate": "2000-01-01",
                            lvis#"IsDefault": "false",
                            lvis#"PolicyCategoryId": "2",
                            lvis#"PolicyCoverageId": "3",
                            lvis#"PolicyCoverageName": "Eagle",
                            lvis#"PolicyId": "332",
                            lvis#"PolicyName": "ALTA Short Form EAGLE Loan Policy",
                            lvis#"ValidRateTypes": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "1",
                                    lvis#"Value": "Basic"
                                  }
                                },
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "35",
                                    lvis#"Value": "Investor Rate"
                                  }
                                }
                              }
                            }
                          }
                        },
                        lvis#"PolicyProduct": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"DefaultRateTypeId": "0",
                            lvis#"EffectiveDate": "2000-01-01",
                            lvis#"IsDefault": "false",
                            lvis#"PolicyCategoryId": "2",
                            lvis#"PolicyCoverageId": "1",
                            lvis#"PolicyCoverageName": "Standard",
                            lvis#"PolicyId": "474",
                            lvis#"PolicyName": "ALTA Short Form Residential Loan Policy",
                            lvis#"ValidRateTypes": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "1",
                                    lvis#"Value": "Basic"
                                  }
                                }
                              }
                            }
                          }
                        },
                        lvis#"PolicyProduct": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"DefaultRateTypeId": "0",
                            lvis#"EffectiveDate": "2000-01-01",
                            lvis#"IsDefault": "false",
                            lvis#"PolicyCategoryId": "2",
                            lvis#"PolicyCoverageId": "2",
                            lvis#"PolicyCoverageName": "Extended",
                            lvis#"PolicyId": "485",
                            lvis#"PolicyName": "ALTA Short Form Residential Loan Policy - Extended",
                            lvis#"ValidRateTypes": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "1",
                                    lvis#"Value": "Basic"
                                  }
                                },
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "35",
                                    lvis#"Value": "Investor Rate"
                                  }
                                }
                              }
                            }
                          }
                        },
                        lvis#"PolicyProduct": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"DefaultRateTypeId": "0",
                            lvis#"EffectiveDate": "2000-01-01",
                            lvis#"IsDefault": "false",
                            lvis#"PolicyCategoryId": "1",
                            lvis#"PolicyCoverageId": "1",
                            lvis#"PolicyCoverageName": "Standard",
                            lvis#"PolicyId": "437",
                            lvis#"PolicyName": "ALTA Standard U.S. Policy",
                            lvis#"ValidRateTypes": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "1",
                                    lvis#"Value": "Basic"
                                  }
                                }
                              }
                            }
                          }
                        },
                        lvis#"PolicyProduct": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"DefaultRateTypeId": "0",
                            lvis#"EffectiveDate": "2000-01-01",
                            lvis#"IsDefault": "false",
                            lvis#"PolicyCategoryId": "2",
                            lvis#"PolicyCoverageId": "3",
                            lvis#"PolicyCoverageName": "Eagle",
                            lvis#"PolicyId": "342",
                            lvis#"PolicyName": "Eagle Loan Policy",
                            lvis#"ValidRateTypes": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "1",
                                    lvis#"Value": "Basic"
                                  }
                                },
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "35",
                                    lvis#"Value": "Investor Rate"
                                  }
                                }
                              }
                            }
                          }
                        },
                        lvis#"PolicyProduct": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"DefaultEndorsementIds": null,
                            lvis#"DefaultRateTypeId": "1",
                            lvis#"EffectiveDate": "2000-01-01",
                            lvis#"IsDefault": "true",
                            lvis#"PolicyCategoryId": "1",
                            lvis#"PolicyCoverageId": "3",
                            lvis#"PolicyCoverageName": "Eagle",
                            lvis#"PolicyId": "429",
                            lvis#"PolicyName": "Eagle Owner's Policy",
                            lvis#"ValidRateTypes": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "1",
                                    lvis#"Value": "Basic"
                                  }
                                },
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "35",
                                    lvis#"Value": "Investor Rate"
                                  }
                                },
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "281",
                                    lvis#"Value": "Short Term Residential Non-Prepaid"
                                  }
                                },
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "282",
                                    lvis#"Value": "Short Term Residential Prepaid"
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    },
                    lvis#"RecordingDocTypes": do {
                      ns lvis http://services.firstam.com/lvis/v2.0
                      ---
                      {
                        lvis#"RecordingDocType": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"ConsiderationAmount": "0",
                            lvis#"ConsiderationLiabilitySource": "Loan Amount",
                            lvis#"DocName": "Mortgage (Deed of Trust)",
                            lvis#"DocType": "MORTGAGE",
                            lvis#"IsDefault": "true",
                            lvis#"NumberOfPages": "25"
                          }
                        },
                        lvis#"RecordingDocType": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"ConsiderationAmount": "0",
                            lvis#"ConsiderationLiabilitySource": "Sales Amount",
                            lvis#"DocName": "Conveyance Deed",
                            lvis#"DocType": "DEED",
                            lvis#"IsDefault": "true",
                            lvis#"NumberOfPages": "3"
                          }
                        },
                        lvis#"RecordingDocType": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"ConsiderationAmount": "0",
                            lvis#"DocName": "Assignment",
                            lvis#"DocType": "ASSIGNMENT",
                            lvis#"IsDefault": "false",
                            lvis#"NumberOfPages": "0"
                          }
                        },
                        lvis#"RecordingDocType": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"ConsiderationAmount": "0",
                            lvis#"DocName": "Satisfaction",
                            lvis#"DocType": "SATISFACTION",
                            lvis#"IsDefault": "false",
                            lvis#"NumberOfPages": "0"
                          }
                        },
                        lvis#"RecordingDocType": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"ConsiderationAmount": "0",
                            lvis#"DocName": "Amendment (Modification)",
                            lvis#"DocType": "AMMENDMENT",
                            lvis#"IsDefault": "false",
                            lvis#"NumberOfPages": "0"
                          }
                        },
                        lvis#"RecordingDocType": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"ConsiderationAmount": "0",
                            lvis#"DocName": "Subordination",
                            lvis#"DocType": "SUBORDINATION",
                            lvis#"IsDefault": "false",
                            lvis#"NumberOfPages": "0"
                          }
                        },
                        lvis#"RecordingDocType": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"ConsiderationAmount": "0",
                            lvis#"DocName": "Power of Attorney",
                            lvis#"DocType": "POA",
                            lvis#"IsDefault": "false",
                            lvis#"NumberOfPages": "0"
                          }
                        },
                        lvis#"RecordingDocType": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"ConsiderationAmount": "0",
                            lvis#"DocName": "UCC1-County",
                            lvis#"DocType": "UCC1COUNTY",
                            lvis#"IsDefault": "false",
                            lvis#"NumberOfPages": "0"
                          }
                        },
                        lvis#"RecordingDocType": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"ConsiderationAmount": "0",
                            lvis#"DocName": "UCC3-County",
                            lvis#"DocType": "UCC3COUNTY",
                            lvis#"IsDefault": "false",
                            lvis#"NumberOfPages": "0"
                          }
                        },
                        lvis#"RecordingDocType": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"ConsiderationAmount": "0",
                            lvis#"DocName": "UCC Termination-County",
                            lvis#"DocType": "UCCTERMINATIONCOUNTY",
                            lvis#"IsDefault": "false",
                            lvis#"NumberOfPages": "0"
                          }
                        },
                        lvis#"RecordingDocType": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"ConsiderationAmount": "0",
                            lvis#"DocName": "Death Certificate",
                            lvis#"DocType": "DEATHCERTIFICATE",
                            lvis#"IsDefault": "false",
                            lvis#"NumberOfPages": "0"
                          }
                        },
                        lvis#"RecordingDocType": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"ConsiderationAmount": "0",
                            lvis#"DocName": "Affidavit",
                            lvis#"DocType": "AFFIDAVIT",
                            lvis#"IsDefault": "false",
                            lvis#"NumberOfPages": "0"
                          }
                        },
                        lvis#"RecordingDocType": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"ConsiderationAmount": "0",
                            lvis#"DocName": "UCC1-State",
                            lvis#"DocType": "UCC1STATE",
                            lvis#"IsDefault": "false",
                            lvis#"NumberOfPages": "0"
                          }
                        },
                        lvis#"RecordingDocType": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"ConsiderationAmount": "0",
                            lvis#"DocName": "UCC3-State",
                            lvis#"DocType": "UCC3STATE",
                            lvis#"IsDefault": "false",
                            lvis#"NumberOfPages": "0"
                          }
                        },
                        lvis#"RecordingDocType": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"ConsiderationAmount": "0",
                            lvis#"DocName": "UCC Termination-State",
                            lvis#"DocType": "UCCTERMINATIONSTATE",
                            lvis#"IsDefault": "false",
                            lvis#"NumberOfPages": "0"
                          }
                        }
                      }
                    },
                    lvis#"SecondPolicyProducts": do {
                      ns lvis http://services.firstam.com/lvis/v2.0
                      ---
                      {
                        lvis#"PolicyProduct": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"DefaultRateTypeId": "0",
                            lvis#"EffectiveDate": "2000-01-01",
                            lvis#"IsDefault": "false",
                            lvis#"PolicyCategoryId": "2",
                            lvis#"PolicyCoverageId": "1",
                            lvis#"PolicyCoverageName": "Standard",
                            lvis#"PolicyId": "469",
                            lvis#"PolicyName": "ALTA Loan Policy",
                            lvis#"ValidRateTypes": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "1",
                                    lvis#"Value": "Basic"
                                  }
                                },
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "35",
                                    lvis#"Value": "Investor Rate"
                                  }
                                }
                              }
                            }
                          }
                        },
                        lvis#"PolicyProduct": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"DefaultEndorsementIds": null,
                            lvis#"DefaultRateTypeId": "1",
                            lvis#"EffectiveDate": "2000-01-01",
                            lvis#"IsDefault": "true",
                            lvis#"PolicyCategoryId": "2",
                            lvis#"PolicyCoverageId": "2",
                            lvis#"PolicyCoverageName": "Extended",
                            lvis#"PolicyId": "470",
                            lvis#"PolicyName": "ALTA Loan Policy - Extended",
                            lvis#"ValidRateTypes": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "1",
                                    lvis#"Value": "Basic"
                                  }
                                },
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "35",
                                    lvis#"Value": "Investor Rate"
                                  }
                                }
                              }
                            }
                          }
                        },
                        lvis#"PolicyProduct": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"DefaultRateTypeId": "0",
                            lvis#"EffectiveDate": "2000-01-01",
                            lvis#"IsDefault": "false",
                            lvis#"PolicyCategoryId": "2",
                            lvis#"PolicyCoverageId": "3",
                            lvis#"PolicyCoverageName": "Eagle",
                            lvis#"PolicyId": "332",
                            lvis#"PolicyName": "ALTA Short Form EAGLE Loan Policy",
                            lvis#"ValidRateTypes": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "1",
                                    lvis#"Value": "Basic"
                                  }
                                }
                              }
                            }
                          }
                        },
                        lvis#"PolicyProduct": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"DefaultRateTypeId": "0",
                            lvis#"EffectiveDate": "2000-01-01",
                            lvis#"IsDefault": "false",
                            lvis#"PolicyCategoryId": "2",
                            lvis#"PolicyCoverageId": "1",
                            lvis#"PolicyCoverageName": "Standard",
                            lvis#"PolicyId": "474",
                            lvis#"PolicyName": "ALTA Short Form Residential Loan Policy",
                            lvis#"ValidRateTypes": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "1",
                                    lvis#"Value": "Basic"
                                  }
                                },
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "35",
                                    lvis#"Value": "Investor Rate"
                                  }
                                }
                              }
                            }
                          }
                        },
                        lvis#"PolicyProduct": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"DefaultRateTypeId": "0",
                            lvis#"EffectiveDate": "2000-01-01",
                            lvis#"IsDefault": "false",
                            lvis#"PolicyCategoryId": "2",
                            lvis#"PolicyCoverageId": "2",
                            lvis#"PolicyCoverageName": "Extended",
                            lvis#"PolicyId": "485",
                            lvis#"PolicyName": "ALTA Short Form Residential Loan Policy - Extended",
                            lvis#"ValidRateTypes": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "1",
                                    lvis#"Value": "Basic"
                                  }
                                },
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "35",
                                    lvis#"Value": "Investor Rate"
                                  }
                                }
                              }
                            }
                          }
                        },
                        lvis#"PolicyProduct": do {
                          ns lvis http://services.firstam.com/lvis/v2.0
                          ---
                          {
                            lvis#"DefaultRateTypeId": "0",
                            lvis#"EffectiveDate": "2000-01-01",
                            lvis#"IsDefault": "false",
                            lvis#"PolicyCategoryId": "2",
                            lvis#"PolicyCoverageId": "3",
                            lvis#"PolicyCoverageName": "Eagle",
                            lvis#"PolicyId": "342",
                            lvis#"PolicyName": "Eagle Loan Policy",
                            lvis#"ValidRateTypes": do {
                              ns lvis http://services.firstam.com/lvis/v2.0
                              ---
                              {
                                lvis#"KeyValue": do {
                                  ns lvis http://services.firstam.com/lvis/v2.0
                                  ---
                                  {
                                    lvis#"Key": "1",
                                    lvis#"Value": "Basic"
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}
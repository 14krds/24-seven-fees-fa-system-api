{
  "headers": {
    "apikey": "a478e6d8-a975-407e-818d-9c264ba76a2c",
    "user-agent": "PostmanRuntime/7.26.1",
    "accept": "*/*",
    "cache-control": "no-cache",
    "postman-token": "3fb7ad56-9134-4280-9ca8-2c91efaec97b",
    "host": "localhost:8082",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive"
  },
  "clientCertificate": null,
  "method": "GET",
  "scheme": "https",
  "queryParams": {
    "state": "NM"
  },
  "requestUri": "/api/detail/propertyTypes?state=NM",
  "queryString": "state=NM",
  "version": "HTTP/1.1",
  "maskedRequestPath": "/detail/propertyTypes",
  "listenerPath": "/api/*",
  "relativePath": "/api/detail/propertyTypes",
  "localAddress": "127.0.0.1:8082",
  "uriParams": {},
  "rawRequestUri": "/api/detail/propertyTypes?state=NM",
  "rawRequestPath": "/api/detail/propertyTypes",
  "remoteAddress": "127.0.0.1:52748",
  "requestPath": "/api/detail/propertyTypes"
}
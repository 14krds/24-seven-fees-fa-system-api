{
  "headers": {
    "apikey": "a478e6d8-a975-407e-818d-9c264ba76a2c",
    "user-agent": "PostmanRuntime/7.26.1",
    "accept": "*/*",
    "cache-control": "no-cache",
    "postman-token": "571266e6-3109-414c-99da-f7d6f9748248",
    "host": "localhost:8082",
    "accept-encoding": "gzip, deflate, br",
    "connection": "keep-alive"
  },
  "clientCertificate": null,
  "method": "GET",
  "scheme": "https",
  "queryParams": {
    "state": "NM",
    "propertyType": "Residential"
  },
  "requestUri": "/api/detail/transactionTypes?state=NM&propertyType=Residential",
  "queryString": "state=NM&propertyType=Residential",
  "version": "HTTP/1.1",
  "maskedRequestPath": "/detail/transactionTypes",
  "listenerPath": "/api/*",
  "relativePath": "/api/detail/transactionTypes",
  "localAddress": "127.0.0.1:8082",
  "uriParams": {},
  "rawRequestUri": "/api/detail/transactionTypes?state=NM&propertyType=Residential",
  "rawRequestPath": "/api/detail/transactionTypes",
  "remoteAddress": "127.0.0.1:52435",
  "requestPath": "/api/detail/transactionTypes"
}
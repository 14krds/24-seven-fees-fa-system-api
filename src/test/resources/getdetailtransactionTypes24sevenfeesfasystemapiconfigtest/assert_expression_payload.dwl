%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo([
  {
    "id": "4",
    "transactionType": "Construction Loan"
  },
  {
    "id": "5",
    "transactionType": "Equity Loan"
  },
  {
    "id": "6",
    "transactionType": "Foreclosure"
  },
  {
    "id": "18",
    "transactionType": "Mortgage Modification"
  },
  {
    "id": "8",
    "transactionType": "Refinance"
  },
  {
    "id": "9",
    "transactionType": "REO Sale w/ Mortgage"
  },
  {
    "id": "10",
    "transactionType": "REO Sale/Cash"
  },
  {
    "id": "11",
    "transactionType": "Sale w/ Mortgage"
  },
  {
    "id": "12",
    "transactionType": "Sale/Cash"
  },
  {
    "id": "13",
    "transactionType": "Sale/Exchange"
  },
  {
    "id": "15",
    "transactionType": "Second Mortgage"
  },
  {
    "id": "17",
    "transactionType": "Short Sale"
  }
])